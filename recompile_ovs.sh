#!/bin/bash

dir=ovs

echo Removing any OVS modules
sudo rmmod openvswitch;

set -e

echo Moving to OVS directory
pushd $dir

echo linking tpp code
pushd datapath
rm -f dmon*
ln -s ../../conga/dmon* .
popd

./boot.sh
./configure --with-linux=/lib/modules/$(uname -r)/build
make -j9

sleep 1

echo Inserting new module
pushd datapath
sudo insmod ./linux/openvswitch.ko
popd

echo deleting ctl and pid files
sudo rm -f /usr/local/var/run/openvswitch/ovs-vswitchd.*
sudo rm -f /usr/local/var/run/openvswitch/*.{ctl,pid}
sudo killall -9 ovsdb-server

echo cleaning old OVS logs
pushd /usr/local/var/log
sudo rm -rf openvswitch
sudo ln -s /var/log/openvswitch
popd

pushd /usr/local/var/run
sudo rm -rf openvswitch
sudo ln -s /var/run/openvswitch
popd

echo Restarting OVS daemons
sudo service openvswitch-switch stop
#sudo service openvswitch-controller stop

confdb=/etc/openvswitch/conf.db

if [ -f $confdb ]; then
	sudo rm $confdb
	sudo ovsdb-tool create $confdb vswitchd/vswitch.ovsschema
fi

if [ -f /usr/local/$confdb ]; then
	sudo rm /usr/local/$confdb
	sudo ovsdb-tool create /usr/local/$confdb vswitchd/vswitch.ovsschema
fi


sudo service openvswitch-switch start
#sudo service openvswitch-controller start
