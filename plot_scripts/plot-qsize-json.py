from plot_defaults import *
import json
import sys

FILE = sys.argv[1]
ret = json.loads(open(FILE, 'r').read())

values = []
values_ts = []

for intf in ret.keys():
    data = np.array(ret[intf]).T
    xs = data[0].astype(float)
    ys = data[1].astype(float)
    values.append(ys)
    values_ts.append((xs, ys))

plt.figure(figsize=(10,8))
plt.subplot(211)
for val in values:
    #plt.plot(ts, val)
    #plt.hist(val)
    plot_ecdf(val)
    plt.ylim((0.5,1))
plt.xlabel('Queue size (packets)')
plt.ylabel('Fractiles')
#plt.savefig('qsize-cdf.pdf')
plt.subplot(212)
#plt.figure(figsize=(10,4))
for idx,(ts,val) in enumerate(values_ts):
    ts = (ts-min(ts)) / 1e6
    alpha = 1 if idx == 0 else 0.4
    lw = 2 if idx == 0 else 1
    plt.plot(ts, val, alpha=alpha, lw=lw)

plt.xlim((2, 3))
plt.xlabel('Time (s)')
plt.ylabel('Queue size (packets)')
plt.tight_layout()
plt.savefig('qsize-ts-2.pdf')
print 'done... qsize-ts-2.pdf'
#plt.show()
