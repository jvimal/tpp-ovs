from plot_defaults import *
import sys
import re

DIR = sys.argv[1]
RULES = [0, 1, 10, 100, 1000]
MODES = ["first", "last", "all"]

pat_ethstats = re.compile(r'.*s1-eth1:\s(.*) Mb/s In')

def parse_log(log, pat):
    lines = open(log).readlines()
    ret = []
    for line in lines:
        m = pat.match(line)
        if m:
            ret.append(float(m.group(1)))
    return ret

def dir(mode, rules):
    return "%s/mode%s-rules%s" % (DIR, mode, rules)

def data2(mode, rules):
    file_name = dir(mode, rules) + "/ethstats.log"
    return parse_log(file_name, pat_ethstats)

plt.figure(figsize=(10,3.5))
markers = 'sv^+x'
markersize = 20
for marker, mode in zip(markers, MODES):
    ys_m, ys_e = [], []
    print mode
    for x in RULES:
        # with TPP, throughput
        ret = data2(mode, x)
        #print x, flow, np.mean(ret), np.std(ret)
        ret = np.array(ret[5:25])/1000.0
        ys_m.append(np.mean(ret))
        ys_e.append(np.std(ret))

    xs = range(len(RULES))
    print xs, ys_m
    xs_str = map(lambda x: '$%s$' % x, [0, 1, 10, 100, 1000])
    plt.errorbar(xs, ys_m, yerr=ys_e, label='%s' % (mode), marker=marker)
    plt.xlabel('Number of rules')
    plt.ylabel('Throughput (Gb/s)')
    plt.xticks(xs, xs_str)
    plt.ylim((0, 10))

plt.legend(ncol=1,frameon=False, loc='lower left')
plt.tight_layout()
outfile = 'tpp-filter-plot.pdf'
plt.savefig(outfile)
print 'saved to', outfile
#plt.show()
