from plot_defaults import *
import sys
import re
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('--dir', required=True)
parser.add_argument('-n', default='1')
parser.add_argument('--out')
FLAGS = parser.parse_args()

DIR = 'rcp-May13--20-29'
#DIR = 'rcp-May14--01-01'
DIR = 'rcp-May14--01-46'
DIR = FLAGS.dir

AGGR = ["maxmin", "prop"]

pat_ethstats = re.compile(r'\s+(.*):\s(.*) Mb/s In')

def parse_log(log, pat):
    lines = open(log).readlines()
    ret = defaultdict(list)
    for line in lines:
        m = pat.match(line)
        if m:
            intf = m.group(1)
            val = float(m.group(2))
            ret[intf].append(val)
    return ret

def dir(aggr):
    return "%s/rcp-%s-n%s" % (DIR, aggr, FLAGS.n)

def data2(aggr):
    file_name = dir(aggr) + "/ethstats.log"
    return parse_log(file_name, pat_ethstats)

plt.figure(figsize=(10,4))
markers = 'sv^+x'
markersize = 20

def plot_aggr(aggr, title):
    ys_m, ys_e = [], []
    ret = data2(aggr)
    labels = ['flow a', 'flow b', 'flow c']
    intfs = ['s1-eth1', 's1-eth2', 's3-eth3']
    for intf,label,marker in zip(intfs, labels, markers):
        arr = ret[intf]
	arr = np.array(arr)[20:]
        #ys_m.append(np.mean(arr))
        #ys_e.append(np.std(arr))
        plt.plot(arr, lw=2, label=label, marker=marker, markevery=5)

    plt.xlabel('Time (s)')
    plt.ylabel('Throughput (Mb/s)')
    plt.gca().set_title(title, fontsize=20)
    plt.ylim((0, 70))

plt.subplot(121)
plot_aggr("maxmin", "Max-min fairness")
plt.subplot(122)
plot_aggr("prop", "Proportional fairness")

plt.subplot(121)
plt.legend(ncol=1,frameon=False, loc='lower right')
plt.tight_layout()

if FLAGS.out:
    outfile = FLAGS.out
    plt.savefig(outfile)
    print 'saved to', outfile
else:
    plt.show()

#plt.show()
