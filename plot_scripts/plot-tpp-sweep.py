from plot_defaults import *
import sys
import re

DIR = sys.argv[1]
SAMPLES = [1, 10, 20, 0]
SAMPLE_LABELS = ['$1$', '$10$', '$20$', '$\infty$']
TPP = [0, 1]
NUM_FLOWS = [1, 10, 20]

pat_iperf_single = re.compile(r'\[\s*\d+\].*sec.*Bytes\s+(.*)Gbits/sec')
pat_iperf_multiple = re.compile(r'\[SUM\].*sec.*Bytes\s+(.*)Gbits/sec')
pat_iperf = [pat_iperf_multiple, pat_iperf_single]
pat_ethstats = re.compile(r'.*s1-eth1:\s(.*) Mb/s In')

def parse_log(log, pat):
    lines = open(log).readlines()
    ret = []
    for line in lines:
        m = pat.match(line)
        if m:
            ret.append(float(m.group(1)))
    return ret

def dir(sample, tpp, num_flows):
    ret = "%s/sample%s-tpp%s-flows%s" % (DIR, sample, tpp, num_flows)
    print ret
    return ret

def data(sample, tpp, num_flows):
    file_name = dir(sample, tpp, num_flows) + "/iperf.log"
    return parse_log(file_name, pat_iperf[num_flows == 1])

def data2(sample, tpp, num_flows):
    file_name = dir(sample, tpp, num_flows) + "/ethstats.log"
    return parse_log(file_name, pat_ethstats)

plt.figure(figsize=(10,4))
markers = 'sv^+x'
markersize = 20
for marker, flow in zip(markers, NUM_FLOWS):
    ys_m, ys_e = [], []
    for x in SAMPLES:
        # with TPP, throughput
        ret = data(x, 1, flow)
        print ret
        #print x, flow, np.mean(ret), np.std(ret)
        m = np.mean(ret)
        ys_m.append(m)
        lo, hi = m - np.percentile(ret, 5), np.percentile(ret, 95) - m
        ys_e.append((lo,hi))

    plt.subplot(121)
    xs = SAMPLES[0:-1] + [30]
    plt.errorbar(xs, ys_m, yerr=np.array(ys_e).T, label='%d flows' % (flow), marker=marker)
    plt.xticks(xs, SAMPLE_LABELS)
    plt.xlabel('Sampling Frequency')
    plt.ylabel('TCP goodput (Gb/s)')
    plt.ylim((2,10))

    plt.subplot(122)
    ys_m, ys_e = [], []
    for x in SAMPLES:
        # with TPP, goodput
        ret = data2(x, 1, flow)
        print ret
        ret = np.array(ret)/1000
        #print x, flow, np.mean(ret), np.std(ret)
        m = np.mean(ret)
        ys_m.append(m)
        lo, hi = m - np.percentile(ret, 5), np.percentile(ret, 95) - m
        ys_e.append((lo,hi))
    xs = SAMPLES[0:-1] + [30]
    plt.errorbar(xs, ys_m, yerr=np.array(ys_e).T, label='%d flows' % (flow), marker=marker)
    plt.xticks(xs, SAMPLE_LABELS)
    plt.xlabel('Sampling Frequency')
    plt.ylabel('Throughput (Gb/s)')
    plt.ylim((2,10))

plt.subplot(121)
plt.legend(loc='lower left',ncol=2,frameon=False, prop={'size':16})
plt.tight_layout()
outfile = 'tpp-insert-plot.pdf'
plt.savefig(outfile)
print 'saved to', outfile
#plt.show()
