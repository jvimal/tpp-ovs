
Before running these scripts, please follow instructions in ../README file to
install the TPP enabled kernel module as well as TPP enabled end-host plugin.
(Steps 2 and 3.)

Otherwise, these experiments will fail below.

Note: many experiments are performance sensitive.  It's best to run these
experiments on a VM with at least 16 CPU cores and 8GB memory in native VM mode
(i.e., qemu-kvm, instead of emulated qemu).

If there is any error in the middle of the experiment, please restart VM and
try again.

Sometimes, ovs-vswitchd process locks up in high CPU util, in which case,
performance will suffer.   You can try restarting the OVS process by running:

    sudo service openvswitch-switch restart

-----

To reproduce experiments/graphs from the paper:

Figure 1
--------

0. cd ~/tpp-ovs/qdisc && ./install.sh

1. cd ~/tpp-ovs/py

2. sudo python queuesize.py --intrusive 1

4. After the program exits, you can plot the graph as follows:

   cd ~/tpp-ovs/plot_scripts; python plot-qsize-json.py ~/tpp-ovs/py/qsize.json
   Your plot should be ready in the filename output by the program.


Figure 2
--------

0. cd ~/tpp-ovs/qdisc && ./install.sh

1. cd ~/tpp-ovs/py

2. sudo ./tpp-rcp-bench.sh

   ... wait for a while until all experiments complete.

3. The results of the experiment should be in plot-rcp.pdf.


Figure 4
--------

0. cd ~/tpp-ovs/qdisc && ./install.sh

1. cd ~/tpp-ovs/py

2. sudo ./tpp-conga-bench.sh

3. Check the results in conga-MonthXX--yy-zz/average_{overhead,rate}.txt.
   The rate should be ~120Mb/s and ~50Mb/s in the average_rate.txt file.

   Note: If even on of the rates doesn't show up as high as 120Mb/s, it means
   the experiment is CPU bound.

Figure 9
--------

0. cd ~/tpp-ovs/py

1. sudo ./tpp-filter-bench.sh

2. sudo ./tpp-insert-bench.sh

Check the respective plots.

These performance numbers might vary with VM overheads.  The plots in the paper
were obtained from a VM with 16 CPU cores running inside qemu in Linux.
