from plot_defaults import *
import sys

FILE = sys.argv[1]
ret = pd.read_csv(open(FILE))

ts = ret.values.T[0]
values = ret.values.T[1:]

plt.figure(figsize=(10,8))
plt.subplot(211)
for val in values:
    #plt.plot(ts, val)
    #plt.hist(val)
    plot_ecdf(val)
    plt.ylim((0.7,1))
plt.xlabel('Queue size (packets)')
plt.ylabel('Fractiles')
#plt.savefig('qsize-cdf.pdf')
plt.subplot(212)
#plt.figure(figsize=(10,4))
for val in values:
    plt.plot(ts, val)
plt.xlabel('Time (s)')
plt.ylabel('Queue size (packets)')
plt.tight_layout()
plt.savefig('qsize-ts.pdf')

print 'done...'
#plt.show()

