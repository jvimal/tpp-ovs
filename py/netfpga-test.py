import tpp
from tpp import Instruction
import argparse
import socket

parser = argparse.ArgumentParser()
parser.add_argument('--dest', default='10.0.0.1')
parser.add_argument('--port', default=10000, type=int)

FLAGS = parser.parse_args()

# The byte offset into the packet (including Ethernet and IP header)
INS_OFFSET = 64

# Ethernet + IP header
HEADER_SIZE = 14 + 20

# The IP payload size
PAYLOAD_SIZE = 100

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

def send():
    for reg in xrange(2):
        bytes = Instruction(tpp.LOAD_REG_HOPMEM, reg, 0).pack()
        data = 'x' * (INS_OFFSET - HEADER_SIZE)
        data += bytes
        data += '\x00' * (PAYLOAD_SIZE - len(data))
        sock.sendto(data, (FLAGS.dest, FLAGS.port))

if __name__ == "__main__":
    send()
