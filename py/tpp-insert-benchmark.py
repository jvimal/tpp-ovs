
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.log import lg, info, setLogLevel
from mininet.util import dumpNodeConnections, quietRun, moveIntf
from mininet.cli import CLI
from mininet.link import TCLink

from subprocess import Popen, PIPE, check_output
from time import sleep, time
from multiprocessing import Process
from argparse import ArgumentParser

import sys
import os
import termcolor as T
import time

setLogLevel('info')

parser = ArgumentParser()
parser.add_argument('-n', type=int, default=2)
parser.add_argument('--dir', default='/tmp')
parser.add_argument('--time', type=int, default=10)
parser.add_argument('--sample-freq', type=int, default=1)
parser.add_argument('--num-flows', type=int, default=1, choices=[1, 10, 20])
parser.add_argument('--tpp', type=int, default=0, choices=[0, 1])
parser.add_argument('--cli', default=False, action="store_true")
FLAGS = parser.parse_args()

class SimpleTopo(Topo):
    def __init__(self):
        super(SimpleTopo, self).__init__()
        for i in xrange(FLAGS.n):
            self.addNode('h%d' % (i+1))
        self.addSwitch('s1')
        for i in xrange(FLAGS.n):
            self.addLink('h%d' % (i+1), 's1')
        return

def main():
    os.system("pgrep -f echo.py | xargs kill -9")

    if not os.path.exists(FLAGS.dir):
        os.makedirs(FLAGS.dir)

    os.system("rmmod sch_tbf; insmod ../qdisc/sch_tbf.ko enable_tpp=%d enable_limit=0 sample_freq=%d" % (FLAGS.tpp, FLAGS.sample_freq))
    net = Mininet(topo=SimpleTopo(), link=TCLink)
    net.start()

    for host in net.hosts:
        #mss = 1100 if FLAGS.tpp else 1460
        mss = 1460
        host.cmd("ip route change 10/8 dev %s-eth0 advmss %s" % (host.name, mss))
        host.waitOutput()
        host.cmd("ifconfig %s-eth0 mtu 2000" % host.name)
        host.waitOutput()
        host.cmd("tc qdisc del dev %s-eth0 root; tc qdisc add dev %s-eth0 root handle 1: tbf rate 10mbit limit 100000 burst 10000" % (host.name, host.name))
        host.waitOutput()

    if FLAGS.cli:
        CLI(net)
    else:
        h1, h2 = net.get('h1'), net.get('h2')
        h2.cmd('iperf -s &')
        h2.waitOutput()

        os.system("ethstats -n1 | grep --line-buffered s1-eth1 > %s/ethstats.log &" % FLAGS.dir)
        time.sleep(1)

        print 'starting iperf...'
        h1.cmd('iperf -c %s -t %s -i 1 -P %s > %s/iperf.log' % (h2.IP(), FLAGS.time, FLAGS.num_flows, FLAGS.dir))
        h1.waitOutput()
    #os.system("mn -c")
    net.stop()
    os.system("killall -9 iperf netserver netperf ethstats")

if __name__ == "__main__":
    main()
