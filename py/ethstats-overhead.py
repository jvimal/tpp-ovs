import sys
import re
from argparse import ArgumentParser
from collections import defaultdict
import numpy as np

parser = ArgumentParser()
parser.add_argument('--files', nargs="+", required=True)
parser.add_argument('--dir', default=0, choices=[0, 1], type=int)

FLAGS = parser.parse_args()

def parse_file(fname):
	# h4-eth0:    2.10 Mb/s In    52.42 Mb/s Out -   1274.0 p/s In    6108.0 p/s Out
	pat_spaces = re.compile(r'\s+')
	ins = defaultdict(list)
	outs = defaultdict(list)
	for line in open(fname, 'r').xreadlines():
		lst = pat_spaces.split(line.strip())
		iface = lst[0]
		if iface.startswith('lo'):
			continue
		inrate = float(lst[1])
		outrate = float(lst[4])
		ins[iface].append(inrate)
		outs[iface].append(outrate)
	iface = ins.keys()[0]
	inavg = np.mean(ins[iface])
	outavg = np.mean(outs[iface])
	if FLAGS.dir == 0:
		if outavg == 0:
			return 0
		return inavg * 100.0/outavg
	else:
		if inavg == 0:
			return 0
		return outavg * 100.0/inavg

if __name__ == "__main__":
	for fname in FLAGS.files:
		try:
			print fname, '%.3f' % parse_file(fname)
		except Exception, e:
			pass
