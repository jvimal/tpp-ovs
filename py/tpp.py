import struct
import socket
import select
import time

DMON_MAGIC = 0xfaceface
DMON_MAGIC_REPLY = 0xfaceb00c
DMON_INSTRUCTION_SIZE_BYTES = 4
DMON_PACKET_MEM_MULTIPLE = 4
DMON_HOP_MEM_MULTIPLE = 8


LOAD_REG_HOPMEM = 0
LOAD_SMEM_HOPMEM = 1
STORE_HOPMEM_REG = 2
STORE_HOPMEM_SMEM = 3
STOREC_HOPMEM_HOPMEM_REG = 4
STOREC_HOPMEM_HOPMEM_SMEM = 5
BITSET_POS_REG = 6
BITSET_POS_SMEM = 7
BITCLEAR_POS_REG = 8
BITCLEAR_POS_SMEM = 9
STOREPUSH_REG = 10
STOREPUSH_SMEM = 11
EXE_IF_REG_MATCHES_PREAMBLE = 12


REG_SW_DPID = 0
REG_SW_FLOWTABLE_VERSION = 1
REG_SW_FLAGS = 2
REG_LINK_RX = 3
REG_LINK_TX = 4
REG_LINK_UTIL = 5
REG_QUEUE_ENQUEUED = 6
REG_PACKET_HOPNUM = 7
REG_PACKET_INPORT = 8
REG_EGRESS_START = 9
REG_EGRESS_PORT = 10
REG_EGRESS_QUEUE_ENQUEUED = 11
REG_EGRESS_RX_RATE = 12
REG_EGRESS_TX_RATE = 13
REG_EGRESS_TX_BYTES = 14
REG_EGRESS_MAX = 15
REG_TSTAMP = 16

class ControlRegs:
    def __init__(self, magic=0, is_programmed=0,
                 hop_mem_size_bytes=0,
                 packet_mem_size_bytes=0,
                 num_instructions=0,
                 preamble_present=0,
                 preamble=0,
                 data=''):
        self.magic = magic
        self.is_programmed = is_programmed
        self.hop_mem_size = hop_mem_size_bytes / DMON_HOP_MEM_MULTIPLE
        self.hop_num = 0
        self.packet_mem_size = packet_mem_size_bytes / DMON_PACKET_MEM_MULTIPLE
        self.packet_mem_size_bytes = packet_mem_size_bytes
        self.num_instructions = num_instructions
        self.preamble_present = preamble_present
        self.preamble = preamble
        self.reserved = 0
        self.data = data

    def unpack(self):
        data = struct.unpack('@IBBBB', self.data)
        self.magic = data[0]
        self.is_programmed = data[1] & 0x1
        self.hop_mem_size = ((data[1] >> 1) & 0b111)
        self.hop_mem_size_bytes = self.hop_mem_size * DMON_HOP_MEM_MULTIPLE
        self.hop_num = (data[1] >> 4)
        self.packet_mem_size = int(data[2])
        self.packet_mem_size_bytes = self.packet_mem_size * DMON_PACKET_MEM_MULTIPLE
        self.num_instructions = (data[3] & 0xf)
        self.preamble_present = (data[3] >> 4)
        return self

    def __str__(self):
        return "ControlRegs<magic=%x,hops=%d,hop_size_bytes=%d>" % (self.magic, self.hop_num, self.hop_mem_size_bytes)

    def pack(self):
        basic = struct.pack('@IBBBB',
                            self.magic,
                            self.is_programmed | (self.hop_mem_size << 1) | (self.hop_num << 4),
                            self.packet_mem_size,
                            self.num_instructions | (self.preamble_present << 4),
                            0)
        extra = ''
        if self.preamble_present:
            extra  = struct.pack('@Q', self.preamble)
        return basic+extra

class Instruction:
    def __init__(self, ins, op0=0, op1=0, op2=0):
        self.ins = ins
        self.op0 = op0
        self.op1 = op1
        self.op2 = op2

    def pack(self):
        ins = self.ins
        ins_enc = struct.pack('B', self.ins)
        args = ''
        if ins == LOAD_REG_HOPMEM:
            args = struct.pack('BB', self.op0, self.op1 << 4)
        elif ins == LOAD_SMEM_HOPMEM:
            args = struct.pack('=HB', self.op0,
                               self.op1 << 4)
        elif ins == STORE_HOPMEM_REG:
            args = struct.pack('=BB', self.op0 << 4, self.op1)
        elif ins == STORE_HOPMEM_SMEM:
            args = struct.pack('=BH', self.op0 << 4, self.op1)
        elif ins == STOREC_HOPMEM_HOPMEM_REG:
            args = struct.pack('=BB', (self.op0 << 4) | self.op1,
                               self.op2)
        elif ins == STOREC_HOPMEM_HOPMEM_SMEM:
            args  = struct.pack('=BH', (self.op0 << 4) | self.op1,
                                self.op2)
        elif ins == BITSET_POS_REG:
            pass
        elif ins == BITSET_POS_SMEM:
            pass
        elif ins == BITCLEAR_POS_REG:
            pass
        elif ins == BITCLEAR_POS_SMEM:
            pass
        elif ins == EXE_IF_REG_MATCHES_PREAMBLE:
            args = struct.pack('B', self.op0 << 4)
        elif ins == STOREPUSH_SMEM:
            pass
        elif ins == STOREPUSH_REG:
            pass
        L = len(ins_enc + args)
        ret = ins_enc + args + '\x00' * (DMON_INSTRUCTION_SIZE_BYTES - L)
        assert(len(ret) == DMON_INSTRUCTION_SIZE_BYTES)
        return ret

class TPP(object):
    def __init__(self, is_programmed=1,
                 hop_mem_size_bytes=40,
                 packet_mem_size_bytes=200,
                 num_instructions=1,
                 preamble_present=0,
                 preamble=0,
                 port=6666,
                 hopdata=[]):
        self.header = ControlRegs(magic=DMON_MAGIC,
                                  is_programmed=1,
                                  hop_mem_size_bytes=hop_mem_size_bytes,
                                  packet_mem_size_bytes=hop_mem_size_bytes * 5,
                                  num_instructions=num_instructions,
                                  preamble_present=preamble_present,
                                  preamble=preamble)

        self.instructions = []
        self.hopdata = hopdata # Array of arrays
        self.sock = socket.socket(socket.AF_INET,
                                  socket.SOCK_DGRAM)
        self.sock.bind(('', port))

    def add_instruction(self, ins):
        self.instructions.append(ins)

    def pack_hopdata(self):
        ret = ''
        for hoparray in self.hopdata:
            ret += ''.join(map(lambda d: struct.pack('@Q', d), hoparray))
        return ret

    def pack(self):
        self.header.num_instructions = len(self.instructions)
        hdr = self.header.pack()
        ins = ''.join(map(lambda ins: ins.pack(), self.instructions))
        #pmem = '\x00' * self.header.packet_mem_size_bytes
        hmem = self.pack_hopdata()
        pmem = '\x00' * (self.header.packet_mem_size_bytes - len(hmem))
        return hdr + ins + hmem + pmem

    def sendto(self, ip, port):
        return self.sock.sendto(self.pack(), (ip, port))

    def flush_input_data(self):
        self.sock.setblocking(0)
        while True:
            try:
                data = self.sock.recv(8192)
            except socket.error:
                break
        return

    def execute(self, ip, port, retry_sec=0.01, max_retries=10):
        inputs = [self.sock]
        BUFF_SIZE_BYTES = 2000
        self.flush_input_data()
        self.sendto(ip, port)
        retry_number = 0
        while retry_number < max_retries:
            retry_number += 1
            inready = []
            try:
                inready, outready,exready = select.select(inputs, [], [], retry_sec)
            except socket.error, e:
                continue

            for s in inready:
                data, addr = s.recvfrom(BUFF_SIZE_BYTES)
                if self.response_ok(data):
                    return self.parse_response(data)

            # Retry...
            self.sendto(ip, port)
        return None

    def response_ok(self, resp):
        regs = ControlRegs(data=resp[0:8]).unpack()
        return regs.magic == DMON_MAGIC_REPLY and regs.hop_num > 0

    def parse_response(self, resp):
        return self.unpack(resp).results

    def unpack(self, data):
        regs = ControlRegs(data=data[0:8]).unpack()
        offset = 8
        if regs.preamble_present:
            regs.preamble = struct.unpack('@Q', data[offset:offset+8])[0]
            offset += 8

        length = regs.num_instructions * DMON_INSTRUCTION_SIZE_BYTES
        instructions = data[offset:offset+length]
        offset += length

        hopdata = data[offset:]
        fmt = '@' + 'Q' * regs.hop_mem_size
        length = DMON_HOP_MEM_MULTIPLE * regs.hop_mem_size
        results = []
        while len(hopdata) > length:
            data = struct.unpack(fmt, hopdata[0:length])
            hopdata = hopdata[length:]
            results.append(data)
        self.results = results[0:regs.hop_num]
        self.header = regs
        return self

    def size(self):
        return len(self.header.pack()) \
            + self.header.num_instructions * DMON_INSTRUCTION_SIZE_BYTES \
            + self.header.packet_mem_size_bytes

def hexdump(string, chunk_size=16):
    ret = []
    L = len(string)
    for i in xrange(0, len(string), chunk_size):
        hexstr = '.'.join(x.encode('hex') \
                              for x in string[i:min(i+chunk_size,L)])
        ret.append(hexstr)

    print '\n'.join(ret)

def HopMem(offset):
    return offset

def SwitchMem(offset):
    return offset

if __name__ == "__main__":
    t = TPP()
    prog = [
        Instruction(LOAD_REG_HOPMEM, REG_EGRESS_QUEUE_ENQUEUED, 0),
        Instruction(LOAD_REG_HOPMEM, REG_EGRESS_PORT, 1),
        Instruction(LOAD_REG_HOPMEM, REG_SW_DPID, 2)
        ]
    for ins in prog:
        t.add_instruction(ins)
    hexdump(t.pack())
