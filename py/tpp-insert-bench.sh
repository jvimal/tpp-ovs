#!/bin/bash

exptid=`date +%b%d--%H-%M`
dir=tpp-ins-$exptid
time=30

# When running inside a VM, if you see several factors
# lower throughput than what you see in the paper,
# check whether the clocksource of the VM has changed
# from tsc (most efficient) to hpet (least efficient).
# cat /sys/devices/system/clocksource/clocksource0/available_clocksource

for sample in 0 1 10 20; do
for tpp in 0 1; do
for num_flows in 1 10 20; do
    python tpp-insert-benchmark.py --dir $dir/sample$sample-tpp$tpp-flows$num_flows \
	--time $time \
	--sample-freq $sample \
	--num-flows $num_flows \
	--tpp $tpp
done
done
done

python ~/tpp-ovs/plot_scripts/plot-ins-sweep.py $dir
