
import os
#import pyzmq
import pcapy
import logging
import argparse
from tpp import *
import time
import sys

logging.basicConfig(level=logging.DEBUG)

parser = argparse.ArgumentParser()
parser.add_argument('--collector-ip', default='10.0.0.1')
parser.add_argument('--collector-port', default=9001, type=int)
parser.add_argument('--dev', default='h1-eth0')
parser.add_argument('--max-len', default=400, type=int)
parser.add_argument('--filter', default='ether proto 0x6666')

FLAGS = parser.parse_args()
PROMISCUOUS = 0
READ_TIMEOUT = 100 #milliseconds
MAX_PACKETS = -1
ETH_HDR_LEN = 14

def to_string(number):
    x = struct.pack('@Q', number)
    y = struct.unpack('b' * 8, x)
    z = map(chr, filter(lambda a: a > 0, y))
    return ''.join(z)

class Timer:
    def __init__(self, interval):
        self.interval = interval
        self.last = time.time()

    def ok(self):
        now = time.time()
        if now - self.last > self.interval:
            self.last = now
            return True
        return False

printguard = Timer(1)
count = 0
size = 0

hop_ds = defaultdict(int)

def cb(hdr, data):
    global count, printguard, size
    count += 1
    eth, tpp = data[0:ETH_HDR_LEN], data[ETH_HDR_LEN:]
    t = TPP()
    ret = t.unpack(tpp)
    if ret.header.hop_num > 0:
        # this TPP has execed
        #print t.regs
        pkt = data[ETH_HDR_LEN + t.size():]
        history = []
        for (port,dpid) in ret.results:
            #history.append((to_string(dpid), port))
            size += 8 + 2
            hop_ds[to_string(dpid)] |= (1 << hash(port))
        size += len(data) - ETH_HDR_LEN - t.size()
        if printguard.ok():
            print history, count, 'packets', size/1e6, 'MB'
            sys.stdout.flush()

def main():
    logging.info("Starting sketch aggregator")
    p = pcapy.open_live(FLAGS.dev, FLAGS.max_len, PROMISCUOUS, READ_TIMEOUT)
    p.setfilter(FLAGS.filter)
    #p.setdirection(pcapy.PCAP_D_IN)
    logging.info("Listening for packets, parameters = %s" % str(vars(FLAGS)))
    p.loop(MAX_PACKETS, cb)

if __name__ == "__main__":
    main()
