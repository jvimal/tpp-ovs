#!/bin/bash

exptid=`date +%b%d--%H-%M`
dir=tpp-filter-$exptid
time=30
num_flows=10

for num_rules in 0 1 10 100 1000; do
for mode in first last all; do
    python tpp-filter-benchmark.py \
	--dir $dir/mode$mode-rules$num_rules \
	--time $time \
	--num-rules $num_rules \
	--num-flows $num_flows \
	--mode $mode
done
done

python ~/tpp-ovs/plot_scripts/plot-tpp-sweep.py $dir
