import asyncore, socket
import argparse
import logging
logging.basicConfig(level=logging.INFO)

parser = argparse.ArgumentParser()
parser.add_argument('--port', type=int, default=10000)
FLAGS = parser.parse_args()

class Server(asyncore.dispatcher):
    def __init__(self):
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET, socket.SOCK_STREAM)
        self.bind(('', FLAGS.port))
        self.listen(100)
        logging.info('Listening...')

    def handle_accept(self):
        sock, addr = self.accept()
        logging.info('Connection from %s' % (addr,))
        ConnectionHandler(sock)

class ConnectionHandler(asyncore.dispatcher_with_send):
    def handle_read(self):
        if not self.recv(1024):
            self.close()

s = Server()
asyncore.loop()
