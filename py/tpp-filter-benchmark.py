
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.log import lg, info, setLogLevel
from mininet.util import dumpNodeConnections, quietRun, moveIntf
from mininet.cli import CLI
from mininet.link import TCLink

from subprocess import Popen, PIPE, check_output
from time import sleep, time
from multiprocessing import Process
from argparse import ArgumentParser

import sys
import os
import termcolor as T
import time

setLogLevel('info')

parser = ArgumentParser()
parser.add_argument('-n', type=int, default=2)
parser.add_argument('--dir', default='/tmp')
parser.add_argument('--time', type=int, default=10)
parser.add_argument('--sample-freq', type=int, default=1)
parser.add_argument('--num-flows', type=int, default=1, choices=[1, 10, 100])
parser.add_argument('--num-rules', type=int, default=1, choices=[0, 1, 10, 100, 1000])
parser.add_argument('--tpp', type=int, default=0, choices=[0, 1])
parser.add_argument('--cli', default=False, action="store_true")
parser.add_argument('--mode', default="first", choices=["first", "last", "all"])
FLAGS = parser.parse_args()

class SimpleTopo(Topo):
    def __init__(self):
        super(SimpleTopo, self).__init__()
        for i in xrange(FLAGS.n):
            self.addNode('h%d' % (i+1))
        self.addSwitch('s1')
        for i in xrange(FLAGS.n):
            self.addLink('h%d' % (i+1), 's1')
        return

def main():
    os.system("pgrep -f echo.py | xargs kill -9")

    if not os.path.exists(FLAGS.dir):
        os.makedirs(FLAGS.dir)

    os.system("rmmod sch_tbf; insmod ../qdisc/sch_tbf.ko enable_tpp=%d enable_limit=0 sample_freq=%d" % (FLAGS.tpp, FLAGS.sample_freq))
    net = Mininet(topo=SimpleTopo(), link=TCLink)
    net.start()

    for host in net.hosts:
        mss = 1100 if FLAGS.tpp else 1460
        host.cmd("ip route change 10/8 dev %s-eth0 advmss %s" % (host.name, mss))
        host.waitOutput()
        host.cmd("tc qdisc del dev %s-eth0 root; tc qdisc add dev %s-eth0 root handle 1: tbf rate 10mbit limit 100000 burst 10000" % (host.name, host.name))
        host.waitOutput()

    if FLAGS.cli:
        CLI(net)
    else:
        h1, h2 = net.get('h1'), net.get('h2')

        # Add iptables rules
        print ' *** adding iptables rules...'
        for i in xrange(FLAGS.num_rules):
            cmd = 'iptables -A OUTPUT -p tcp --dport %d -j MARK --set-mark %d' % (5001 + i, i+1)
            h1.cmd(cmd)

        num_iperfs = FLAGS.num_rules
        if FLAGS.num_rules == 0:
            num_iperfs = 1

        print 'starting iperf servers...'
        for i in xrange(num_iperfs):
            h2.cmd('iperf -s -p %d &' % (i + 5001))

        os.system("ethstats -n1 | grep --line-buffered s1-eth1 > %s/ethstats.log &" % FLAGS.dir)
        time.sleep(1)

        print 'starting iperf clients...'
        if FLAGS.mode == "first":
            h1.cmd('iperf -c %s -t %s -i 1 -P %s -p %d > %s/iperf.log 2>&1' % (h2.IP(), 10*FLAGS.time, FLAGS.num_flows, 5001, FLAGS.dir))
        elif FLAGS.mode == "last":
            lastport = 5000 + FLAGS.num_rules
            if FLAGS.num_rules == 0:
                lastport = 5001
            h1.cmd('iperf -c %s -t %s -i 1 -P %s -p %d > %s/iperf.log 2>&1' % (h2.IP(), 10*FLAGS.time, FLAGS.num_flows, lastport, FLAGS.dir))
        elif FLAGS.mode == "all":
            num_flows = FLAGS.num_flows
            if FLAGS.num_rules > 0:
                num_flows = FLAGS.num_flows / FLAGS.num_rules
                if num_flows == 0:
                    num_flows = 1
            procs = []

            num_iperfs = FLAGS.num_rules
            if FLAGS.num_rules == 0:
                num_iperfs = 1

            for i in xrange(num_iperfs):
                proc = h1.popen('iperf -c %s -t %s -i 1 -P %d -p %d > %s/iperf.log 2>&1' % (h2.IP(), 10*FLAGS.time, num_flows, 5001+i, FLAGS.dir), shell=True)
                procs.append(proc)
            print 'waiting for all iperfs to complete...'
            for proc in procs:
                proc.wait()

        out = h1.cmd('iptables -L -v')

        with open(FLAGS.dir + "/iptables-count", 'w') as f:
            print >>f, out
            print out[0:100]
            print 'saving counters...'

    os.system("killall -9 iperf netserver netperf ethstats")
    os.system("mn -c")

if __name__ == "__main__":
    try:
        main()
    except Exception, e:
        print T.colored(e, "yellow")
        os.system("killall -9 iperf netserver netperf ethstats")
        os.system("mn -c")
