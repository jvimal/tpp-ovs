import socket
import argparse
import os
import select
import tpp
import sys
import struct

parser = argparse.ArgumentParser()
parser.add_argument('--port', type=int, default=5001)
FLAGS = parser.parse_args()
PPID = os.getppid()
TIMEOUT_SEC = 1
BUFF_SIZE_BYTES = 2000

def loop():
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(('', FLAGS.port))
    inputs = [sock]
    while True:
        inputready = []
        try:
            inputready, outputready, exceptready = select.select(inputs, [], [], TIMEOUT_SEC)
        except socket.error, e:
            print 'socket error..', e
            break

        if os.getppid() != PPID:
            print 'parent exited..'
            return

        for s in inputready:
            data, addr = s.recvfrom(BUFF_SIZE_BYTES)
            magic = struct.unpack('@I', data[0:4])
            if magic[0] == tpp.DMON_MAGIC:
                data = struct.pack('@I', tpp.DMON_MAGIC_REPLY) + data[4:]
                s.sendto(data, addr)

loop()
