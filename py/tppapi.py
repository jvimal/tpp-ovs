
import os
from tpp import *

# This is the interface with which we talk to the kernel.  Its implementation
# is in sch_tbf.c.  You can search for 'add_tpp' in the sch_tbf.c kernel module
# code to see how we add the TPP to existing packets.

FILE = '/sys/module/sch_tbf/parameters/add_tpp'
assert(os.path.exists(FILE))

def add_tpp(devname, mark, sample_frequency, tpp_bytes):
    string = "%s %d %d %d %s" % (devname, mark, sample_frequency, len(tpp_bytes), tpp_bytes)
    print "writing '%s' to %s" % (repr(string), FILE)
    with open(FILE, 'w') as f:
        f.write(string)
    return

def del_tpp(devname, mark):
    string = "%s %d" % (devname, mark)
    print "writing '%s' to %s" % (repr(string), FILE)
    with open(FILE, 'w') as f:
        f.write(string)
    return

if __name__ == "__main__":
    t = TPP(hop_mem_size_bytes=3 * 8)
    t.instructions = [
        Instruction(LOAD_REG_HOPMEM, REG_EGRESS_QUEUE_ENQUEUED, HopMem(0)),
        Instruction(LOAD_REG_HOPMEM, REG_EGRESS_PORT, HopMem(1)),
        Instruction(LOAD_REG_HOPMEM, REG_SW_DPID, HopMem(2)),
        ]

    add_tpp('h1-eth0', mark=1, sample_frequency=1, tpp_bytes=t.pack())
