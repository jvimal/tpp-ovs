#!/bin/bash

exptid=`date +%b%d--%H-%M`
dir=conga-$exptid
# dir=rcp-tmp

function average_rate {
	egrep 's1-eth3' ethstats*.log | awk '{ sum += $5 } END { print sum/NR }'
	egrep 's1-eth4' ethstats*.log | awk '{ sum += $5 } END { print sum/NR }'
}

function average_overhead {
	grep overhead *.log | awk '{ print $10 }' | cut -d '%' -f 1 | awk '{ sum+=$1 } END { print sum/NR }'
}

function run {
	python conga-network.py \
	    --dir $dir \
	    --time 100 \
	    --rate 200 \
	    --cross_rate 50 \
	    --demand 120

	pushd $dir
	average_overhead > average_overhead.txt
    average_rate > average_rate.txt
	popd
}

run
