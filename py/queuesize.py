
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.log import lg, info, setLogLevel
from mininet.util import dumpNodeConnections, quietRun, moveIntf
from mininet.cli import CLI
from mininet.link import TCLink

from subprocess import Popen, PIPE, check_output
from time import sleep, time
from multiprocessing import Process
from argparse import ArgumentParser

import sys
import os
import termcolor as T
import time
import tppapi
from tpp import *
import traceback

setLogLevel('info')

parser = ArgumentParser()
parser.add_argument('--htb', action="store_true", default=False)
parser.add_argument('--tcp', action="store_true", default=False)
parser.add_argument('-n', type=int, default=3)
parser.add_argument('--bw', type=float, default=100.0)
parser.add_argument('--intrusive', default=0, type=int, help="Sample rate if intrusive")
FLAGS = parser.parse_args()

class Dumbell(Topo):
    def __init__(self):
        super(Dumbell, self).__init__()
        for i in xrange(2 * FLAGS.n):
            self.addNode('h%d' % (i+1))
        self.addSwitch('s1')
        self.addSwitch('s2')
        for i in xrange(FLAGS.n):
            self.addLink('h%d' % (i+1), 's1', bw=FLAGS.bw)
        for i in xrange(FLAGS.n, 2*FLAGS.n):
            self.addLink('h%d' % (i+1), 's2', bw=FLAGS.bw)
        self.addLink('s1', 's2', bw=FLAGS.bw)
        return

def main():
    os.system("pgrep -f echo.py | xargs kill -9")
    net = Mininet(topo=Dumbell(), link=TCLink)
    net.start()
    t = TPP(hop_mem_size_bytes=3 * 8)
    t.instructions = [
        Instruction(LOAD_REG_HOPMEM, REG_EGRESS_QUEUE_ENQUEUED, HopMem(0)),
        Instruction(LOAD_REG_HOPMEM, REG_TSTAMP, HopMem(1)),
        Instruction(LOAD_REG_HOPMEM, REG_SW_DPID, HopMem(2)),
        ]

    tccmds = [
        'tc qdisc del dev %s-eth0 root',
        'tc qdisc add dev %s-eth0 root handle 1: tbf rate 10mbit limit 100000 burst 10000',
        ]

    # Start traffic generator
    ips = []
    for host in net.hosts:
        host.cmd('python trafserver.py &')
        host.waitOutput()
        ips.append(host.IP())

        if FLAGS.intrusive:
            host.cmd('iptables -F')
            host.waitOutput()
            host.cmd('iptables -A OUTPUT -p tcp --dport 10000 -j MARK --set-mark 1')
            host.waitOutput()
            host.cmd("ip route change 10/8 dev %s-eth0 advmss 1300" % host.name)
            host.waitOutput()

            # Add the tbf qdisc
            for cmd in tccmds:
                host.cmd(cmd % host.name)
                host.waitOutput()

            print '*** adding TPP to %s-eth0' % host.name
            tppapi.add_tpp('%s-eth0' % host.name, 1, FLAGS.intrusive, t.pack())
    time.sleep(1)
    for h1 in net.hosts:
        h1.cmd('python trafclient.py --dest %s --parallel 5 &' % (' '.join(ips)))
        h1.waitOutput()
        h1.popen('python echo.py >/tmp/%s.log' % (h1.name), shell=True)

    print 'started traffic client...'
    # Start the queue monitor

    if not FLAGS.intrusive:
        h1 = net.get('h1')
        h1.cmd('pwd; python tpp-qsize.py --num-hosts %s --time 20 > qsize.txt' % len(net.hosts))
        h1.waitOutput()
    else:
        print 'commands to try:'
        print 'h1 python aggregator.py --out qsize.json'
        print 'while true; do tc -s qdisc show > qsize.txt; sleep 0.1; done'
        # CLI(net)
        h1.cmd('python aggregator.py --out qsize.json &')
        print 'waiting for queue outputs...'
        time.sleep(10)
    for pat in ['trafserver.py', 'trafclient.py', 'echo.py']:
        os.system("pgrep -f %s | xargs kill -9" % pat)
    os.system("killall -9 iperf netserver netperf")
    os.system("mn -c")

if __name__ == "__main__":
    try:
        main()
    except Exception, e:
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print "*** print_exception:"
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                                  limit=2, file=sys.stdout)
        os.system("mn -c")
