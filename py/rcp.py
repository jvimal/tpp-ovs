
from tpp import *
import socket
import argparse
import time
import sys
import thread

parser = argparse.ArgumentParser()
parser.add_argument('--host', default='10.0.0.2')
parser.add_argument('--port', type=int, default=5001)
parser.add_argument('--src-port', type=int, default=5002)
parser.add_argument('--time', type=float, default=10)
parser.add_argument('--capacity-mbps', type=float, default=10.0)
parser.add_argument('--irate', type=float, default=1)
parser.add_argument('--interval', type=float, default=0.01)
FLAGS = parser.parse_args()

RCP_MEM_OFFSET = 10
RCP_RFAIR_MBPS = FLAGS.irate
MSG_SIZE = 1200
BURST_MSG = 10
MSG = '1234' * (MSG_SIZE / 4)

# Prec is the precision with which we measure rates.
PREC = 1000.0
ALPHA = 0.5
BETA = 0.05

class Timer:
    def __init__(self, interval):
        self.interval = interval
        self.last = time.time()

    def ok(self):
        now = time.time()
        if now - self.last > self.interval:
            self.last = now
            return True
        return False

def packet_size(msg_size):
    # + udp header + IP header + ethernet header
    return msg_size + 8 + 20 + 14


"""
Phase 1 of RCP: Get all the required information from the network.
"""
t1 = TPP(hop_mem_size_bytes=4 * 8)
t1.instructions = [
    Instruction(LOAD_REG_HOPMEM, REG_EGRESS_QUEUE_ENQUEUED, HopMem(0)),
    Instruction(LOAD_REG_HOPMEM, REG_SW_DPID, HopMem(1)),
    Instruction(LOAD_REG_HOPMEM, REG_EGRESS_TX_RATE, HopMem(2)),
    Instruction(LOAD_SMEM_HOPMEM, RCP_MEM_OFFSET, HopMem(3)),
    ]


"""
Phase 3 of RCP: Commit data (using mask + conditions)
"""
t2 = TPP(hop_mem_size_bytes=2 * 8,
         #peamble_present=1,
         #preamble=0, # concatenation of (mask, value)
         hopdata=[ [0, 0], [0, 0], [0, 0], [0, 0] ],
         port=6667)
t2.instructions = [
    #Instruction(EXE_IF_REG_MATCHES_PREAMBLE, REG_SW_DPID),
    Instruction(STOREC_HOPMEM_HOPMEM_SMEM, HopMem(0), HopMem(1), SwitchMem(RCP_MEM_OFFSET)),
    ]

"""
Phase 3 of RCP: Commit data without any conditions.
"""
t3 = TPP(hop_mem_size_bytes=2 * 8,
         hopdata=[ [1, 0], [1, 0], [1, 0], [1, 0] ],
         port=6669)
t3.instructions = [
    Instruction(STORE_HOPMEM_SMEM, HopMem(0), SwitchMem(RCP_MEM_OFFSET)),
    ]

"""
This TPP can be used for debugging.  It simply retrieves
RCP register from the network.
"""
t4 = TPP(hop_mem_size_bytes=2 * 8,
         #hopdata=[ [1, 0], [1, 0], [1, 0], [1, 0] ],
         port=6670)
t4.instructions = [
    Instruction(LOAD_SMEM_HOPMEM, SwitchMem(RCP_MEM_OFFSET), HopMem(0)),
    ]

def sleep_sec():
    global RCP_RFAIR_MBPS
    bits = MSG_SIZE * BURST_MSG * 8
    rate_mbps = RCP_RFAIR_MBPS
    usec = bits * 1.0 /rate_mbps
    sec = usec * 1e-6
    return sec

def sending_thread(addr, port):
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.bind(('', FLAGS.src_port))
    while True:
        for i in xrange(BURST_MSG):
            prog = t2.pack()
            sock.sendto(prog + MSG[0:MSG_SIZE - len(prog)], (addr, port))
        time.sleep(sleep_sec())

def clamp_range(value, lo, hi):
    value = min(hi, value)
    value = max(lo, value)
    return value

def rcp_compute(data, printok=False):
    commit_hopdata = []
    rfairs = []
    rfairs_set = []

    # Compute rfair for each hop in the network
    for hopdata in data:
        queue, dpid, txrate_bps, rfair_mbps = hopdata
        rfair_mbps_orig = rfair_mbps

        rfair_mbps /= PREC

        # Clamp rfair to this range
        rfair_mbps = clamp_range(rfair_mbps, 0.01, FLAGS.capacity_mbps)

        # To set
        rfairs_set.append(rfair_mbps)

        # To compute
        excess = (txrate_bps/1e6 - FLAGS.capacity_mbps)/FLAGS.capacity_mbps
        queue_bytes = queue * 1500.0 * 8 * 1e-6 / FLAGS.interval / FLAGS.capacity_mbps

        if printok:
            print excess, queue_bytes, rfair_mbps, rfair_mbps * (1.0 - ALPHA * excess - BETA * queue_bytes)

        rfair_mbps = rfair_mbps * (1.0 - ALPHA * excess - BETA * queue_bytes)
        rfair_mbps = clamp_range(rfair_mbps, 0.01, FLAGS.capacity_mbps)
        rfairs.append(rfair_mbps)

        commit_hopdata.append([rfair_mbps_orig,
                               int(rfair_mbps * PREC)])
        continue
    return (commit_hopdata, rfairs, rfairs_set)

def rcp_thread():
    global RCP_RFAIR_MBPS
    start = time.time()
    output_guard = Timer(interval=1)
    set_guard = Timer(interval=0.001)
    update_guard = Timer(interval=0.01)
    while True:
        # Phase 1: Get all required data from the network
        data = t1.execute(FLAGS.host, FLAGS.port)
        if data:
            ok = output_guard.ok()
            if ok:
                print 'phase 1 got', data

            # Phase 2: Compute RCP equation for every hop
            commit_hopdata, rfairs, rfairs_set = rcp_compute(data, ok)

            if ok:
                print 'phase 2 rfairs =', rfairs
                print 'network rfairs =', rfairs_set

            # Always set the latest rfair in the network.  We can also
            # set it to the harmonic mean -- this will lead to a
            # proportionally fair allocation (See Raina's paper)
            set_rate = min(rfairs_set)

            if set_guard.ok():
                RCP_RFAIR_MBPS = set_rate
                if ok:
                    print 'setting rate to', set_rate

            # Phase 3: Commit to the network
            if update_guard.ok():
                t2.hopdata = commit_hopdata
                d3 = t2.execute(FLAGS.host, FLAGS.port)
                if ok:
                    print 'setting rate to', set_rate
                    print 'phase 3 got', d3
                    print 'phase 3 program', commit_hopdata
                    hexdump(t2.pack_hopdata())
                    print '*************************'
                    sys.stdout.flush()
        time.sleep(0.001)
        continue
    return

thread.start_new_thread(sending_thread, (FLAGS.host, FLAGS.port,))
thread.start_new_thread(rcp_thread, ())

while True:
    time.sleep(100)
