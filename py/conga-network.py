
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.log import lg, info, setLogLevel
from mininet.util import dumpNodeConnections, quietRun, moveIntf
from mininet.cli import CLI
from mininet.link import TCLink
from mininet.node import RemoteController

from subprocess import Popen, PIPE, check_output
from time import sleep, time
from multiprocessing import Process
from argparse import ArgumentParser

import sys
import os
import signal
import termcolor as T
import time
from tpp import *

def progress(t):
    while t > 0:
        print T.colored('  %3d seconds left  \r' % (t), 'cyan'),
        t -= 1
        sys.stdout.flush()
        sleep(1)
    print '\r\n'

setLogLevel('info')

parser = ArgumentParser()
parser.add_argument('--rate', default=100, type=int)
parser.add_argument('--time', default=30, type=int)
parser.add_argument('--host-rate', default=200, type=int)
parser.add_argument('--cross_paths', default=1, type=int, help="Number of paths the cross flow uses")
parser.add_argument('--cross_rate', default=40, type=int, help="Rate of cross flow")
parser.add_argument('--dir', default=None)
parser.add_argument('--demand', default=120)
FLAGS = parser.parse_args()

"""
Example invocations:

    sudo python conga-network.py
        --rate 100
        --host-rate 200
        --cross_rate 50
        --dir out

"""

class SimpleTopo(Topo):
    def __init__(self):
        super(SimpleTopo, self).__init__()
        self.addNode('h1')
        self.addNode('h2')
        self.addNode('h3')
        self.addNode('h4')
        self.addSwitch('s1')
        self.addSwitch('s2')
        self.addSwitch('s3')
        self.addSwitch('s4')
        links='h1,s1;h2,s1;s1,s2;s1,s3;s2,s4;s3,s4;s4,h3;s4,h4'
        for link in links.split(';'):
            src, dst = link.split(',')
            rate = FLAGS.rate
            if src.startswith('h') or dst.startswith('h'):
                rate = FLAGS.host_rate
            self.addLink(src, dst, bw=rate)
        return

def setupRoutes(net):
    def getIP(node):
        try:
            return net.get(node).IP()
        except:
            # Raw IP
            return node
    def getLink(sw, dst):
        # Gets the link number between src and dst
        # at switch sw
        src = sw
        sw = net.get(sw)
        for idx, link in enumerate(sw.intfList()):
            if link.link:
                u, v = link.link.intf1.name, link.link.intf2.name
                # print u, v, src, dst
                if u.startswith(src) and v.startswith(dst):
                    return idx
                if u.startswith(dst) and v.startswith(src):
                    return idx
        return None

    routes = [
        # switch, src node, dst node, udp port, nexthop
        's1,h1,h3,8001,s2',
        's1,h1,h3,8002,s3',
        's1,h1,h4,8001,s2',
        's1,h2,h4,8001,s2',
        's1,h2,h4,8002,s3',
        's1,0/0,h1,0/0,h1',
        's1,0/0,h2,0/0,h2',

        's2,0/0,h1,0/0,s1',
        's2,0/0,h2,0/0,s1',
        's2,0/0,h3,0/0,s4',
        's2,0/0,h4,0/0,s4',

        's3,0/0,h1,0/0,s1',
        's3,0/0,h2,0/0,s1',
        's3,0/0,h3,0/0,s4',
        's3,0/0,h4,0/0,s4',

        's4,0/0,h3,0/0,h3',
        's4,0/0,h4,0/0,h4',
        's4,0/0,h1,0/0,s2',
        's4,0/0,h2,0/0,s3',
    ]

    for route in routes:
        sw, src, dst, port, nexthop = route.split(',')
        opts = '%s idle_timeout=0,priority=65535,dl_type=0x0800' % sw
        cmd = 'ovs-ofctl add-flow %s,nw_src=%s,nw_dst=%s,nw_proto=17,tp_dst=%s,actions=output:%s'
        link = getLink(sw, nexthop)
        cmd = cmd % (opts, getIP(src), getIP(dst), port, link)
        cmd = cmd.replace('nw_src=0/0,','')
        cmd = cmd.replace('tp_src=0/0,','')
        print cmd
        os.system(cmd)

        cmd = 'ovs-ofctl add-flow %s,nw_dst=%s,nw_proto=1,actions=output:%s' % (opts, getIP(dst), link)
        print cmd
        os.system(cmd)

    # drop ipv6
    for sw in net.switches:
        cmd = 'ovs-ofctl add-flow %s idle_timeout=0,dl_type=0x86dd,actions=drop' % sw.name
        print cmd
        os.system(cmd)

def main():
    net = Mininet(topo=SimpleTopo(), link=TCLink, autoStaticArp=True)
    net.start()
    setupRoutes(net)
    print '*******************************'
    print 'commands:'
    print 'h2 hping3 -2 -p 8002 h4'
    print 'h1 ping h2'
    print 'h2 ../conga/client -dest 10.0.0.4'
    print 'h2 ../conga/client -dest 10.0.0.4 -v 1 -rate_mbps 120 -aggregate max'
    print '*******************************'
    for host in 'h3,h4'.split(','):
        for port in '8001,8002'.split(','):
            cmd = '../conga/server -port %s' % port
            print host, cmd
            net.get(host).popen(cmd, shell=True)
    cmd = "../conga/client -dest 10.0.0.3 -num_paths %d -rate_mbps %s -v 1 > /dev/null 2>&1 " % (FLAGS.cross_paths, FLAGS.cross_rate)
    net.get('h1').popen(cmd, shell=True)
    if FLAGS.dir is None:
        CLI(net)
    else:
        if not os.path.exists(FLAGS.dir):
            os.makedirs(FLAGS.dir)
        cmd = "../conga/client -dest 10.0.0.4 -v 2 -rate_mbps %s -aggregate max > %s/out.log 2>&1" % (FLAGS.demand, FLAGS.dir)
        print cmd
        net.get('h2').popen(cmd, shell=True)
        Popen("ethstats -n1 > %s/ethstats.log" % FLAGS.dir, shell=True)
        progress(FLAGS.time)
    net.stop()
    os.system("killall -9 ehtstats server client")
    return

if __name__ == "__main__":
    main()
