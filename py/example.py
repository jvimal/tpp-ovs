
from tpp import *
from tpp_executor import *

import socket
import argparse
import time
import sys

parser = argparse.ArgumentParser()
parser.add_argument('--host', default='10.0.0.2')
parser.add_argument('--port', type=int, default=5001)
parser.add_argument('--time', type=float, default=10)
parser.add_argument('--interval', type=float, default=0.1)
FLAGS = parser.parse_args()

t1 = TPP(hop_mem_size_bytes=4 * 8)
t1.instructions = [
    Instruction(LOAD_REG_HOPMEM, REG_EGRESS_QUEUE_ENQUEUED, 0),
    Instruction(LOAD_REG_HOPMEM, REG_EGRESS_PORT, 1),
    Instruction(LOAD_REG_HOPMEM, REG_SW_DPID, 2),
    Instruction(LOAD_REG_HOPMEM, REG_EGRESS_TX_RATE, 3),
    ]

t2 = TPP(hop_mem_size_bytes=2 * 8, port=6678)
t2.instructions = [
    Instruction(LOAD_SMEM_HOPMEM, 10, HopMem(0)),
    ]

#hexdump(t.pack())
#print t.sendto('10.0.0.2', 5001)
start = time.time()

ex = TPPExecutor(tpps=[t1, t2], dests=[(FLAGS.host, FLAGS.port)] * 2)

while time.time() - start < FLAGS.time:
    #data = t.execute(FLAGS.host, FLAGS.port)
    data = ex.execute()
    print data
    if data:
        print data
    else:
        print 'no response :('
    sys.stdout.flush()
    time.sleep(FLAGS.interval)
