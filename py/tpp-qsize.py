
from tpp import *
from tpp_executor import *

import socket
import argparse
import time
import sys

parser = argparse.ArgumentParser()
parser.add_argument('--num-hosts', type=int, required=True)
parser.add_argument('--port', type=int, default=5001)
parser.add_argument('--time', type=float, default=10)
parser.add_argument('--interval', type=float, default=0.1)
FLAGS = parser.parse_args()

tpps = []
dests = []

for i in xrange(FLAGS.num_hosts):
    dests.append( ('10.0.0.%d' % (i+1), FLAGS.port) )
    t = TPP(hop_mem_size_bytes=4 * 8, port=9000 + i)
    t.instructions = [
        Instruction(LOAD_REG_HOPMEM, REG_EGRESS_QUEUE_ENQUEUED, 0),
        Instruction(LOAD_REG_HOPMEM, REG_EGRESS_PORT, 1),
        Instruction(LOAD_REG_HOPMEM, REG_SW_DPID, 2),
        Instruction(LOAD_REG_HOPMEM, REG_EGRESS_TX_RATE, 3),
        ]
    tpps.append(t)

start = time.time()

def clean(data):
    ret = []
    for d in data:
        if d is None:
            ret.append(0)
        else:
            ret.append(d[0][0])
    return ret

ex = TPPExecutor(tpps=tpps, dests=dests)
while time.time() - start < FLAGS.time:
    #data = t.execute(FLAGS.host, FLAGS.port)
    before = time.time()
    data = ex.execute()
    after = time.time()
    #print data
    if data:
        dt = after - before
        #print before, after, dt, clean(data)
        print "%s,%s" % (after-start, ','.join(map(lambda s: str(s), clean(data))))
    else:
        print 'no response :('
    sys.stdout.flush()
    time.sleep(FLAGS.interval)
