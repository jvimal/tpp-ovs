
import os
#import pyzmq
import pcapy
import logging
import argparse
from tpp import *
import time
import sys
from collections import defaultdict
import json

logging.basicConfig(level=logging.DEBUG)

parser = argparse.ArgumentParser()
parser.add_argument('--collector-ip', default='10.0.0.1')
parser.add_argument('--collector-port', default=9001, type=int)
parser.add_argument('--dev', default='h1-eth0')
parser.add_argument('--max-len', default=400, type=int)
parser.add_argument('--filter', default='ether proto 0x6666')
parser.add_argument('--out', default='/dev/null')

FLAGS = parser.parse_args()
PROMISCUOUS = 0
READ_TIMEOUT = 100 #milliseconds
MAX_PACKETS = -1
ETH_HDR_LEN = 14

def to_string(number):
    x = struct.pack('@Q', number)
    y = struct.unpack('b' * 8, x)
    z = map(chr, filter(lambda a: a > 0, y))
    return ''.join(z)

class Timer:
    def __init__(self, interval):
        self.interval = interval
        self.last = time.time()

    def ok(self):
        now = time.time()
        if now - self.last > self.interval:
            self.last = now
            return True
        return False

printguard = Timer(1)
flushguard = Timer(5)
count = 0
size = 0
qsizes = defaultdict(list)

def cb(hdr, data):
    global count, printguard, size, qsizes, flushguard
    count += 1
    eth, tpp = data[0:ETH_HDR_LEN], data[ETH_HDR_LEN:]
    t = TPP()
    ret = t.unpack(tpp)
    if ret.header.hop_num > 0:
        # this TPP has execed
        #print t.regs
        pkt = data[ETH_HDR_LEN + t.size():]
        history = []
        for (qsize,ts,dpid) in ret.results:
            dpstr = to_string(dpid)
            history.append((dpstr, qsize))
            ts = '%.3f' % (ts/1e3)
            qsizes[dpstr].append((ts, qsize))
            size += 8 + 2 + 2
        size += len(data) - ETH_HDR_LEN - t.size()
        if printguard.ok():
            print history, count, 'packets', size/1e6, 'MB'
            sys.stdout.flush()

        if flushguard.ok():
            with open(FLAGS.out, 'w') as f:
                json.dump(qsizes, f)
                print ' >>>>> Flushed queue sizes to disk (%s)...' % (FLAGS.out)

def main():
    logging.info("Starting qsize aggregator")
    p = pcapy.open_live(FLAGS.dev, FLAGS.max_len, PROMISCUOUS, READ_TIMEOUT)
    p.setfilter(FLAGS.filter)
    #p.setdirection(pcapy.PCAP_D_IN)
    logging.info("Listening for packets, parameters = %s" % str(vars(FLAGS)))
    p.loop(MAX_PACKETS, cb)

if __name__ == "__main__":
    main()
