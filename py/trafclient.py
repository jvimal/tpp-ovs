import socket
import argparse
import logging
from random import choice
import time
import select

logging.basicConfig(level=logging.INFO)

parser = argparse.ArgumentParser()
parser.add_argument('--dest', nargs="+", required=True)
parser.add_argument('--flow-size-kb', type=float, default=10)
parser.add_argument('--rate-mbps', type=float, default=30)
parser.add_argument('--port', type=int, default=10000)
parser.add_argument('--parallel', type=int, default=10)

FLAGS = parser.parse_args()

data = 'x' * int(FLAGS.flow_size_kb * 1024)
interval_sec = len(data) * 8.0 / FLAGS.rate_mbps * 1e-6
logging.info("Interval: %.6fs" % interval_sec)

socks = []
for dest in FLAGS.dest:
    for p in xrange(FLAGS.parallel):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #sock.setblocking(0)
        sock.connect((dest, FLAGS.port))
        socks.append(sock)

while True:
    inp, out, err = select.select([], socks, [])
    if len(out):
        sock = choice(out)
        sock.send(data)
        time.sleep(interval_sec)
