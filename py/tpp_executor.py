
import select
import socket
import tpp

BUFF_SIZE_BYTES = 8192

class TPPExecutor:
    def __init__(self, tpps=[], dests=[], num_retries=20, retry_interval_sec=0.0001):
        self.tpps = tpps
        self.dests = dests
        self.num_retries = num_retries
        self.retry_interval_sec = retry_interval_sec

    def execute(self):
        # Execute all TPPs returning the responses in the same order
        # of TPPs.
        ret = [ None ] * len(self.tpps)
        retries = [0] * len(self.tpps)
        sockets = [t.sock for t in self.tpps]
        socket_to_index = {}
        for i, sock in enumerate(sockets):
            socket_to_index[sock] = i
        # Flush input from all the TPP sockets
        for tpp in self.tpps:
            tpp.flush_input_data()

        while True:
            finished = True
            for i, tpp in enumerate(self.tpps):
                # Are we done?
                if ret[i] is not None:
                    continue

                # Have we retried enough?
                if retries[i] > self.num_retries:
                    continue

                # Send TPP again
                tpp.sendto(*self.dests[i])
                retries[i] += 1

                # At least one TPP outstanding so we aren't done yet
                finished = False
                continue

            # Wait for responses
            try:
                inready, outready, exready = select.select(sockets, [], [], self.retry_interval_sec)
            except socket.error, e:
                continue

            # Look at those sockets which have a response
            for sock in inready:
                index = socket_to_index[sock]
                tpp = self.tpps[index]
                data, addr = sock.recvfrom(BUFF_SIZE_BYTES)
                if tpp.response_ok(data):
                    ret[index] = tpp.parse_response(data)

            # And finally...
            if finished:
                break
        return ret
