
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.log import lg, info, setLogLevel
from mininet.util import dumpNodeConnections, quietRun, moveIntf
from mininet.cli import CLI
from mininet.link import TCLink

from subprocess import Popen, PIPE, check_output
from time import sleep, time
from multiprocessing import Process
from argparse import ArgumentParser

import sys
import os
import termcolor as T
import time
from tpp import *
import tppapi

setLogLevel('info')

parser = ArgumentParser()
parser.add_argument('--htb', action="store_true", default=False)
parser.add_argument('--rate', default=100, type=int)
parser.add_argument('--tcp', action="store_true", default=False)
parser.add_argument('--sample', type=int, default=10)
FLAGS = parser.parse_args()

class SimpleTopo(Topo):
    def __init__(self):
        super(SimpleTopo, self).__init__()
        self.addNode('h1')
        self.addNode('h2')
        self.addNode('h3')
        self.addSwitch('s1')
        self.addSwitch('s2')
        self.addLink('h1', 's1')
        self.addLink('h2', 's1')
        self.addLink('h3', 's2')
        self.addLink('s1', 's2')
        return

def setup_ndb_tpp(net):
    iptables = [
        'iptables -F',
        'iptables -A OUTPUT -p tcp -j MARK --set-mark 1'
        ]

    t = TPP(hop_mem_size_bytes=2 * 8)
    t.instructions = [
        Instruction(LOAD_REG_HOPMEM, REG_EGRESS_PORT, HopMem(0)),
        Instruction(LOAD_REG_HOPMEM, REG_SW_DPID, HopMem(1)),
        ]

    print 'Setting up iptables/filters'
    for host in net.hosts:
        for cmd in iptables:
            host.cmd(cmd)
            host.waitOutput()

    print 'tpp is'
    hexdump(t.pack())
    print '*' * 80
    for host in net.hosts:
        intf = '%s-eth0' % host.name
        print 'adding tpp to %s' % intf
        tppapi.add_tpp(intf, mark=1, sample_frequency=FLAGS.sample, tpp_bytes=t.pack())

    return

def main():
    os.system("pgrep -f echo.py | xargs kill -9")
    net = Mininet(topo=SimpleTopo(), link=TCLink)
    net.start()
    tccmds = [
        'tc qdisc del dev %s root',
        'tc qdisc add dev %s root handle 1: htb default 1',
        'tc class add dev %s classid 1:1 parent 1: htb rate ' + '%sMbit' % (FLAGS.rate),
        'tc class add dev %s classid 1:10 parent 1:1 htb rate ' + '%sMbit ceil %sMbit' % (FLAGS.rate/2, FLAGS.rate),
        'tc class add dev %s classid 1:11 parent 1:1 htb rate ' + '%sMbit ceil %sMbit' % (FLAGS.rate/2, FLAGS.rate),
        'tc filter add dev %s parent 1: protocol ip prio 1 u32 match ip protocol 17 0xff flowid 1:10',
        'tc filter add dev %s parent 1: protocol ip prio 2 u32 match ip protocol 6 0xff flowid 1:11',
        ]

    for host in net.hosts:
        host.cmd("tc qdisc del dev %s-eth0 root; tc qdisc add dev %s-eth0 root handle 1: tbf rate 10mbit limit 100000 burst 10000" % (host.name, host.name))
        host.waitOutput()
        host.cmd("ip route change 10/8 dev %s-eth0 advmss 1100" % host.name)
        host.waitOutput()
        host.popen("python ~/py/echo.py 2>&1 >/tmp/%s.log" % host.name, shell=True)

    if FLAGS.htb:
        for sw in net.switches:
            for intf in sw.intfs.values():
                if not intf.name.startswith('s'):
                    continue

                print 'installing htb on %s' % intf.name
                for cmd in tccmds:
                    cmd = cmd % intf.name
                    sw.cmd(cmd)
                    sw.waitOutput()
    setup_ndb_tpp(net)

    h3 = net.get('h3')
    h3.popen('iperf -s', shell=True)
    sleep(1)
    if FLAGS.tcp:
        net.get('h1').popen('iperf -c %s -t 1000 -i 1 > /tmp/iperf-h1.log' % h3.IP(), shell=True)
        net.get('h2').popen('iperf -c %s -t 1000 -i 1 > /tmp/iperf-h2.log' % h3.IP(), shell=True)
        print 'starting iperfs...'
    print '*' * 80
    print 'common commands'
    print 'h1 python ndb-aggregator.py'
    print '*' * 80
    CLI(net)
    os.system("pgrep -f echo.py | xargs kill -9")
    os.system("pgrep -f rcp.py | xargs kill -9")
    os.system("pgrep -f ndb-agg | xargs kill -9")
    os.system("killall -9 iperf netserver netperf")
    os.system("mn -c")

if __name__ == "__main__":
    main()
