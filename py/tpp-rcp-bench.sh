#!/bin/bash

exptid=`date +%b%d--%H-%M`
dir=rcp-$exptid
# dir=rcp-tmp

function average_rate {
	for f in h*.log; do
		grep avg $f | awk '{ print $8 }' | sed 's/Mb.*//g' | awk '{ sum += $1 } END { print sum/NR }';
	done
}

function average_overhead {
	grep overhead *.log | awk '{ print $12 }' | cut -d '%' -f 1 | awk '{ sum+=$1 } END { print sum/NR }'
}

function run_rcp {
	for n in 1 10; do
	for algo in maxmin prop; do
        mkdir -p $dir/rcp-$algo-n$n
		python rcp-network.py --topo park \
		    -e --auto \
		    --dir $dir/rcp-$algo-n$n \
		    --aggregate $algo \
		    --rcp-beta 0.3 \
		    --time 100 \
		    --rate 100 \
		    --gamma 0.9 \
		    -n $n \
		    --commit_rcp true \
		    --phase1_interval 1

		pushd $dir/rcp-$algo-n$n
		average_rate | sudo cat > avg_rate.txt
		average_overhead | sudo cat > average_overhead.txt
		popd
		sync
	done
	done	
}

function run_tcp {
	for n in 1 10; do
		python rcp-network.py --topo park \
		    -e --auto \
		    --dir $dir/tcp-n$n \
		    -n $n \
		    --flow tcp
	done
}

run_rcp
run_tcp

python ~/tpp-ovs/plot_scripts/plot-rcp-sweep.py --dir $dir --out plot-rcp.pdf
