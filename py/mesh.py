
from tpp import *
from tpp_executor import *

import socket
import argparse
import time
import sys

parser = argparse.ArgumentParser()
parser.add_argument('--hosts', nargs="+", default=[])
parser.add_argument('--num-hosts', type=int, default=3)
parser.add_argument('--port', type=int, default=5001)
#parser.add_argument('--time', type=float, default=10)
parser.add_argument('--interval', type=float, default=1)
parser.add_argument('--carbon-server', default='10.0.0.1')
parser.add_argument('--carbon-port', default=2003, type=int)
FLAGS = parser.parse_args()

if len(FLAGS.hosts) == 0:
    assert(FLAGS.num_hosts > 0 and FLAGS.num_hosts < (1 << 24))
    for i in xrange(1, 1+FLAGS.num_hosts):
        ip = '10.%d.%d.%d' % ((i >> 16) & 0xff, (i >> 8) & 0xff, i & 0xff)
        FLAGS.hosts.append(ip)
else:
    FLAGS.num_hosts = len(FLAGS.hosts)

tpps = []
for i in xrange(FLAGS.num_hosts):
    t1 = TPP(hop_mem_size_bytes=4 * 8, port=7000+i)
    t1.instructions = [
        Instruction(LOAD_REG_HOPMEM, REG_EGRESS_QUEUE_ENQUEUED, 0),
        Instruction(LOAD_REG_HOPMEM, REG_EGRESS_PORT, 1),
        Instruction(LOAD_REG_HOPMEM, REG_SW_DPID, 2),
        Instruction(LOAD_REG_HOPMEM, REG_EGRESS_TX_RATE, 3),
        ]
    tpps.append(t1)

start = time.time()
ex = TPPExecutor(tpps=tpps,
                 dests=[(host, FLAGS.port) for host in FLAGS.hosts],
                 num_retries=1)

def stats_hopdata(hopdata):
    ret = []
    for hop in hopdata:
        q, port, dpid, rate = hop
        t = int(time.time())
        ret.append("network.switch%d.port%d.rate_bps %d %d" % (dpid, port, rate, t))
        ret.append("network.switch%d.port%d.packets_queued %d %d" % (dpid, port, q, t))
    return ret

sock = socket.socket()
try:
    sock.connect((FLAGS.carbon_server, FLAGS.carbon_port))
except:
    print "Couldn't connect to carbon @%s:%s" % (FLAGS.carbon_server, FLAGS.carbon_port)
    sys.exit(-1)

while True:
    #data = t.execute(FLAGS.host, FLAGS.port)
    data = ex.execute()
    if data:
        pipe = []
        #print data
        for d in data:
            if d is None:
                continue
            try:
                tmp = stats_hopdata(d)
            except:
                pass
            pipe += tmp
        msg = '\n'.join(pipe)
    else:
        print 'no response :('

    #send_to_carbon(msg)
    print msg
    sock.sendall(msg + "\n")
    sys.stdout.flush()
    time.sleep(FLAGS.interval)
