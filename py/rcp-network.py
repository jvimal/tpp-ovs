
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.log import lg, info, setLogLevel
from mininet.util import dumpNodeConnections, quietRun, moveIntf
from mininet.cli import CLI
from mininet.link import TCLink
from mininet.node import RemoteController

from subprocess import Popen, PIPE, check_output
from time import sleep, time
from multiprocessing import Process
from argparse import ArgumentParser

import sys
import os
import signal
import termcolor as T
import time
from tpp import *

def progress(t):
    while t > 0:
        print T.colored('  %3d seconds left  \r' % (t), 'cyan'),
        t -= 1
        sys.stdout.flush()
        sleep(1)
    print '\r\n'

setLogLevel('info')

parser = ArgumentParser()
parser.add_argument('--rate', default=100, type=int)
parser.add_argument('--gamma', default=0.95, type=float)
parser.add_argument('-n', default=1, type=int)
parser.add_argument('--topo', default='simple', choices=['simple', 'park'])
parser.add_argument('-e', action="store_true", default=False)
parser.add_argument('--auto', action="store_true", default=False)
parser.add_argument('--dir', default='/tmp')
parser.add_argument('--aggregate', default='maxmin', choices='maxmin,prop'.split(','))
parser.add_argument('--time', default=50, type=int)
parser.add_argument('--rcp-beta', default=0.3)
parser.add_argument('--commit_rcp', default="false", choices=["true", "false"])
parser.add_argument('--phase1_interval', default=5, type=int, help="in milliseconds")
parser.add_argument('--flow', default='rcp', choices=["tcp", "rcp"], help="Run TCP/RCP expt?")
usage = """
Example invocations:

* Do everything automatically:
  sudo python rcp-network.py --topo park -e --auto --dir test --aggregate prop

* Open a CLI: (-e ensures servers start)
  sudo python rcp-network.py --topo park -e

"""

parser.usage = parser.format_usage() + usage
FLAGS = parser.parse_args()

class SimpleTopo(Topo):
    def __init__(self):
        super(SimpleTopo, self).__init__()
        self.addNode('h1')
        self.addNode('h2')
        self.addNode('h3')
        self.addSwitch('s1')
        self.addSwitch('s2')
        self.addLink('h1', 's1', bw=FLAGS.rate)
        self.addLink('h2', 's1', bw=FLAGS.rate)
        self.addLink('h3', 's2', bw=FLAGS.rate)
        self.addLink('s1', 's2', bw=FLAGS.rate)
        return

class ParkingLot(Topo):
    def __init__(self):
        super(ParkingLot, self).__init__()
        for i in xrange(6):
            self.addNode('h%d' % (i+1))
        for i in xrange(4):
            self.addSwitch('s%d' % (i+1))
        links = ['h1,s1',
                 'h2,s1',
                 's1,s2',
                 's2,s3',
                 's3,s4',
                 's2,h3',
                 'h4,s3',
                 's4,h5',
                 's4,h6']
        for link in links:
            a,b = link.split(',')
            self.addLink(a, b, bw=FLAGS.rate)

def main():
    topos = dict(simple=SimpleTopo(), park=ParkingLot())
    # Start floodlight controller
    controller_cmd = ("killall -9 java iperf client server; java -jar ~/floodlight/target/floodlight.jar > %s/floodlight.log 2>&1;" % FLAGS.dir)
    controller = Popen(controller_cmd, shell=True, preexec_fn=os.setsid)
    net = Mininet(topo=topos[FLAGS.topo], link=TCLink)
    net.start()
    net.pingAll()
    dumpNodeConnections(net.hosts)
    servers = 'h6,h3,h5'.split(',')
    clients = 'h1,h2,h4'.split(',')
    print '*' * 80
    print 'common commands'
    if FLAGS.topo == 'simple':
        print 'h2 ../rcp/server -v 1 &'
        print 'h1 ../rcp/client -dest 10.0.0.2 -rcp_capacity_mbps %d' % (FLAGS.rate * FLAGS.gamma)
    else:
        for server in servers:
            if FLAGS.flow == "rcp":
                cmd = '../rcp/server -v 1 > /tmp/%s.log 2>&1' % server
                print '%s %s &' % (server, cmd)
            else:
                cmd = "iperf -s > /tmp/%s.log 2>&1" % server
                print '%s %s &' % (server, cmd)
            if FLAGS.e:
                net.get(server).popen(cmd, shell=True)
        for i,client in enumerate(clients):
            print '%s ../rcp/client -dest %s -v 1' % (client, net.get(servers[i]).IP())
    print '*' * 80

    if FLAGS.auto:
        os.system("ethstats -n1 > %s/ethstats.log &" % FLAGS.dir)
        for (c,s) in zip(clients,servers):
            client = net.get(c)
            server = net.get(s)
            for i in xrange(FLAGS.n):
                if FLAGS.flow == "rcp":
                    args = (server.IP(), FLAGS.rcp_beta, FLAGS.aggregate, FLAGS.rate * FLAGS.gamma, FLAGS.commit_rcp, FLAGS.phase1_interval, FLAGS.dir, c, i)
                    client.popen("../rcp/client -dest %s -v 1 -rcp_beta %s -aggregate %s -rate_mbps 1 -rcp_capacity_mbps %s -commit_rcp %s -phase1_interval %s > %s/%s-%d.log 2>&1" % args,
                               shell=True)
                else:
                    args = (server.IP(), FLAGS.dir, c, i)
                    client.popen("iperf -c %s -t 1000 -i 1 > %s/%s-%d.log 2>&1" % args, shell=True)
            client.popen("ethstats -n1 > %s/ethstats-%s.log" % (FLAGS.dir, c), shell=True)
            server.popen("ethstats -n1 > %s/ethstats-%s.log" % (FLAGS.dir, s), shell=True)
	try:
            progress(FLAGS.time)
	except KeyboardInterrupt:
            pass
    else:
        CLI(net)
    os.system("killall -9 iperf client server ethstats")
    #controller.kill()
    os.killpg(controller.pid, signal.SIGTERM)
    net.stop()

if __name__ == "__main__":
    try:
        if not os.path.exists(FLAGS.dir):
            os.makedirs(FLAGS.dir)
        main()
    except Exception, e:
        print e
        os.system("mn -c")
