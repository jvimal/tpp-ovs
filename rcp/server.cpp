#include <cstdio>
#include <string>
using namespace std;

#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <errno.h>
#include <unistd.h>
#include <sys/unistd.h>
#include <sys/fcntl.h>
#include <time.h>
#include <glog/logging.h>
#include <gflags/gflags.h>
#include <arpa/inet.h>

#include "dmon2_lib.c"
#include "tpp.cpp"

#define NSEC_PER_SEC (1000000000LLU)
#define DMON_REPLY (0xfaceb00c)

DEFINE_int32(port, 10000, "Port");

u64 time_nsec() {
        struct timespec t;
        clock_gettime(CLOCK_MONOTONIC, &t);
        return t.tv_sec * NSEC_PER_SEC + t.tv_nsec;
}

u64 time_usec() {
	return time_nsec() / 1000;
}

void write_total(int sock, int num_bytes) {
	static const int write_size = 4096;
	static char buff[write_size] = {'x'};
	while (num_bytes > 0) {
		size_t num_written = write(sock, buff, min(num_bytes, write_size));
		VLOG(3) << "Number of bytes written: " << num_written;
		if (num_written > 0) {
			num_bytes -= num_written;
		} else {
			perror("write");
			LOG(INFO) << "error, write returned " << num_written;
			break;
		}
	}
}

void write_buffer(int sock, u8 *buff, size_t num) {
	while (num > 0) {
		auto written = write(sock, buff, num);
		if (written > 0) {
			num -= written;
			buff += written;
		} else {
			perror("write()");
			break;
		}
	}
}

void write_to(int sock, u8 *buff, size_t num, struct sockaddr_in addr)
{
	while (num > 0) {
		auto written = sendto(sock, buff, num,
				      0, (struct sockaddr *)&addr, sizeof(addr));
		if (written > 0) {
			num -= written;
			buff += written;
		} else {
			perror("write()");
			break;
		}
	}
}

void read_stuff(int sock) {
	static const int read_size = 4096;
	static u8 buff[read_size];
	struct sockaddr_in from;
	socklen_t from_len;

	while (true) {
		auto num_read = recvfrom(sock, buff, read_size,
					 0, (struct sockaddr *)&from, &from_len);

		/* Process the message */
		TPP tpp(buff);
		if (!tpp.Valid()) {
			VLOG(4) << "Invalid tpp...";
			continue;
		}

		/* Valid TPP, so reflect it */
		auto sz = tpp.MaxSize();
		tpp.cr->magic = DMON_REPLY;
		VLOG(4) << "Reflecting TPP size: " << sz << " " << from.sin_family;
		write_to(sock, tpp.Bytes(), sz, from);
	}
}

void loop(int sock) {
	struct sockaddr_in addr;
	u64 last_usec;
	u32 bytes;

	bzero(&addr, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(FLAGS_port);
	addr.sin_addr.s_addr = INADDR_ANY;

	CHECK(bind(sock, (struct sockaddr *)&addr, sizeof(addr)) == 0) << "Bind failed";
	LOG(INFO) << "Listening on port: " << FLAGS_port;

	while (true) {
		read_stuff(sock);
	}
}

int main(int argc, char *argv[])
{
	int optval;
	google::InitGoogleLogging(argv[0]);
	google::ParseCommandLineFlags(&argc, &argv, true);
	FLAGS_logtostderr = true;

	int sock = socket(AF_INET, SOCK_DGRAM, 0);
	CHECK(sock >= 0) << "Couldn't create socket";
	// CHECK(fcntl(sock, F_SETFL, O_NONBLOCK) == 0) << "Couldn't set non blocking on socket";
	CHECK(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) == 0) << "Error setting reuse address";

	loop(sock);

	return 0;
}
