#ifndef __RCP_H__
#define __RCP_H__
#include <list>
using namespace std;

#include <gflags/gflags.h>

DEFINE_double(rcp_interval_msec, 1, "RCP controller interval");
DEFINE_double(rcp_capacity_mbps, 100.0, "RCP link capacity");
DEFINE_double(rcp_ewma_gain, 0.5, "For averaging link util and qsizes");
DEFINE_double(mtu, 1400.0, "Pkts->bytes conversion");
DEFINE_double(rcp_alpha, 0.5, "alpha parameter");
DEFINE_double(rcp_beta, 0.3, "beta parameter");
DEFINE_double(rcp_inflate, 10.0, "RCP inflation factor for capacity");

struct RCPScheduler
{
	double capacity;
	u64 prev_rate;
	double rate;
	double util;
	double lo;
	double qsize;
	double rtt_ms;

	double avg_util;
	double avg_qsize_kbits;
	bool valid;
	list<double> qsizes;

	RCPScheduler() {
		capacity = FLAGS_rcp_capacity_mbps;
		rate = capacity;
		util = 0;
		lo = 0.001;
		rtt_ms = FLAGS_rcp_interval_msec;

		avg_util = 0;
		avg_qsize_kbits = 0;
		valid = false;
	}

	void set_capacity(double c, double mc=1) {
		capacity = c;
		rate = c/2.0;
	}

	void set_rate(double rate) {
		this->rate = rate;
		valid = true;
	}

	double get_rate(double w=1) {
		return w * rate;
	}

	double get_capacity() {
		return capacity;
	}

	double get_util() {
		return util;
	}

	double set_rtt(double rtt_ms) {
		this->rtt_ms = rtt_ms;
	}

	void update_avg(double util, double qsize_pkts) {
		auto g = FLAGS_rcp_ewma_gain;
		double qsize_kbits = qsize_pkts * FLAGS_mtu * 8.0 / 1024;

		avg_util = util * g + avg_util * (1.0 - g);
		avg_qsize_kbits = qsize_kbits * g + avg_qsize_kbits * (1.0 - g);
		qsizes.push_back(qsize_kbits);
		if (qsizes.size() > 5) {
			qsizes.pop_front();
		}
	}

	double get_persistent_queue() {
		double ret = 1e10;
		for (auto q: qsizes) {
			ret = min(ret, q);
		}
		return ret;
	}

	double compute(u64 prev_rate) {
		this->prev_rate = prev_rate;
		this->rate = prev_rate/1e6;
		compute(avg_util, get_persistent_queue(), this->rtt_ms);
	}

	double compute(double util, double qsize_kbits, double rtt_ms) {
		double excess = util - capacity;
		double excess_drain_mbps = 0;
		if (rtt_ms > 0) {
			excess_drain_mbps = (qsize_kbits) / rtt_ms;
			//printf("qsize: %.3lf, rtt: %.3lf, excess_drain: %.3lf\n",
			//       qsize_kbits, rtt_ms, excess_drain_mbps);
		}

		this->util = util;
		rate = rate * (1.0 - FLAGS_rcp_alpha * excess / capacity - FLAGS_rcp_beta * excess_drain_mbps / capacity);
		//printf("--- %.3lf, %.3lf\n", alpha*excess/capacity, beta*excess_drain_mbps/capacity);
		rate = min(rate, FLAGS_rcp_inflate * capacity);
		rate = max(rate, lo * capacity);
		return rate;
	}
};

#endif /* __RCP_H__ */
