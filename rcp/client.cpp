#include <cstdio>
#include <string>
using namespace std;

#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <errno.h>
#include <unistd.h>
#include <sys/unistd.h>
#include <sys/fcntl.h>
#include <time.h>
#include <glog/logging.h>
#include <gflags/gflags.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>

#include "dmon2_lib.c"
#include "tpp.cpp"
#include "common.h"
#include "rcp.h"

#define NSEC_PER_SEC (1000000000LLU)
#define DMON_REPLY (0xfaceb00c)

DEFINE_string(dest, "10.0.0.1", "Destination to connect to");
DEFINE_int32(port, 10000, "Port");
DEFINE_int32(sleep_usec, 100, "Burst to send in one shot");
DEFINE_double(rate_mbps, 1, "rate in Mb/s");
DEFINE_int32(phase1_interval, 5, "Phase 1 interval in msec");
DEFINE_int32(phase3_interval, 10, "Phase 3 interval in msec");
DEFINE_int32(print_interval, 1000, "Print interval in msec");
DEFINE_string(aggregate, "maxmin", "Aggregation function");
DEFINE_bool(disable_rcp, false, "Disable RCP?");
DEFINE_string(commit_rcp, "true", "Dont commit rate into the network.");
DEFINE_int32(send_bytes, 0, "Send bytes for TPP");

u64 hop_offset(int offset) {
	return offset;
}

u64 switch_mem(int addr) {
	return addr;
}

u64 time_nsec() {
        struct timespec t;
        clock_gettime(CLOCK_MONOTONIC, &t);
        return t.tv_sec * NSEC_PER_SEC + t.tv_nsec;
}

u64 time_usec() {
	return time_nsec() / 1000;
}

void SetPhase1Interval(double srtt_msec) {
	static u32 min_interval = 5;
	static u32 max_interval = 1000;
	FLAGS_phase1_interval = static_cast<u32>(srtt_msec);
	FLAGS_phase1_interval = max(min_interval, (u32)FLAGS_phase1_interval);
	FLAGS_phase1_interval = min(max_interval, (u32)FLAGS_phase1_interval);
}

void write_buffer(int sock, u8 *buff, size_t num) {
	while (num > 0) {
		auto written = write(sock, buff, num);
		if (written > 0) {
			num -= written;
			buff += written;
		} else {
			perror("write()");
			break;
		}
	}
}

void write_total(int sock, int num_bytes) {
	static const int write_size = 1400;
	static u8 buff[write_size] = {'x'};
	static int header_overhead = 8 + 20 + 14;

	int num_packets = (FLAGS_mtu - 1 + num_bytes) / FLAGS_mtu;
	num_bytes -= num_packets * header_overhead;

	while (num_bytes > 0) {
		auto towrite = min(num_bytes, write_size);
		write_buffer(sock, buff, towrite);
		num_bytes -= min(num_bytes, towrite);
	}
}


Timer printphase;
Timer rcpphase;
RCPScheduler rcp_link[MAX_HOPS];
int bytes_data = 0;
int bytes_tpp = 0;
int bytes_tpp_ack = 0;
double srtt_msec = 0;

double aggregate_rates(vector<double> &rfairs) {
	double ret = 3.14159;

	if (rfairs.size() == 0) {
		return FLAGS_rcp_capacity_mbps;
	}

	if (FLAGS_aggregate == "maxmin") {
		ret = 1e9;
		/* min ... */
		for (auto r: rfairs) {
			ret = min(ret, r);
		}
	} else if (FLAGS_aggregate == "prop") {
		ret = 0;
		/* Harmonic mean */
		int num = 0;
		for (auto r: rfairs) {
			if (r == 0)
				continue;
			ret += 1.0 / r;
			num += 1;
		}
		ret = 1.0 / ret;
	}

	return ret;
}

void read_stuff(int sock) {
	static const int read_size = 4096;
	static u8 buff[read_size];
	vector<double> rfair_hops;
	static int last_send_bytes = 0;

	while (true) {
		int count;
		ioctl(sock, FIONREAD, &count);
		if (count == 0)
			break;

		VLOG(4) << "Reading...";
		auto num_read = read(sock, buff, read_size);

		/* Process the message */
		TPP tpp(buff);
		if (!tpp.Valid())
			continue;

		bytes_tpp_ack += tpp.MaxSize();
		if (tpp.Reply()) {
			if (tpp.NumInstructions() != 4)
				continue;

			rfair_hops.clear();
			VLOG(4) << "TPP reply with " << tpp.NumInstructions() << " ins.";
			bool ok = printphase.elapsed_msec(FLAGS_print_interval);
			bool rok = rcpphase.elapsed_msec(FLAGS_phase1_interval);
			double rtt_msec = (time_nsec() - *tpp.GetAuxData(0)) / 1e6;
			srtt_msec = srtt_msec * 0.8 + rtt_msec * 0.2;
			SetPhase1Interval(srtt_msec);

			for (int i = 0; i < tpp.NumHops(); i++) {
				if (ok) {
					for (int j = 0; j < tpp.NumDataPerHop(); j++) {
						VLOG(1) << " hop[" << i << "][" << j << "] = " << tpp.ReadHopdata(i, j);
					}
				}

				auto util = tpp.ReadHopdata(i, 2);
				auto qsize_pkt = tpp.ReadHopdata(i, 0);
				/* Read the rfair already committed in the network */
				auto rfair = tpp.ReadHopdata(i, 3);

				rcp_link[i].update_avg(util/1e6, qsize_pkt);
				rcp_link[i].set_rtt(srtt_msec);

				if (rok) {
					if (FLAGS_commit_rcp[0] == 't') {
						/* If we commit to network, use the network rfair */
						rcp_link[i].compute(rfair);
					} else {
						/* We just use the previous rfair */
						rcp_link[i].compute(rcp_link[i].get_rate());
					}

					// rfair_set = min(rcp_link[i].get_rate(), rfair_set);
					rfair_hops.push_back(rcp_link[i].get_rate());
				}

				if (ok) {
					VLOG(1) << Sprintf("hop[%d]: rfair prev=%.3lf,next=%.3lf,cap=%.3lf,util=%.3lfMb/s,qsize=%.3lfkB",
							   i, rfair/1e6, rcp_link[i].get_rate(), rcp_link[i].capacity,
							   rcp_link[i].avg_util, rcp_link[i].get_persistent_queue()/8.0);
				}
			}

			if (ok) {
				float overhead = (bytes_tpp + bytes_tpp_ack) * 100.0;
				overhead /= (0.0001 + bytes_tpp + bytes_data + bytes_tpp_ack);
				VLOG(1) << Sprintf("bytes_tpp: %d, bytes_data: %d, bytes_tpp_ack: %d, overhead: %.3f%%, srtt: %.3lf\n",
							bytes_tpp, bytes_data, bytes_tpp_ack, overhead, srtt_msec);
				float send_rate = (bytes_tpp + bytes_data - last_send_bytes) * 8.0 / (FLAGS_print_interval * 1000.0);
				last_send_bytes = bytes_tpp + bytes_data;
				VLOG(1) << Sprintf("avg sending rate: %.3fMb/s\n", send_rate);
			}

			if (rfair_hops.size()) {
				auto rfair_set = aggregate_rates(rfair_hops);
				FLAGS_rate_mbps = rfair_set;

				/* Pacing... */
				if (rfair_set < 1)
					FLAGS_sleep_usec = 100 * 1000;
				else if (rfair_set < 20)
					FLAGS_sleep_usec = 10 * 1000;
				else
					FLAGS_sleep_usec = 100;
			}

			if (ok) {
				VLOG(1) << Sprintf("*********** aggregated(%s) = %.3lf",
						   FLAGS_aggregate.c_str(), FLAGS_rate_mbps);
			}
		} else {
			/* Valid TPP, so reflect it */
			tpp.cr->magic = DMON_REPLY;
			write_buffer(sock, tpp.packet, tpp.Size());
		}
	}
}

void write_phase1_tpp(int sock)
{
	TPP ph1(4 * 8,
		FLAGS_max_hops /* max-hops */,
		4 /* number of instr. */);
	ph1.AddInstruction(LOAD_REG_HOPMEM,
			   REG_EGRESS_QUEUE_ENQUEUED,
			   hop_offset(0));
	ph1.AddInstruction(LOAD_REG_HOPMEM,
			   REG_SW_DPID,
			   hop_offset(1));
	ph1.AddInstruction(LOAD_REG_HOPMEM,
			   REG_EGRESS_TX_RATE,
			   hop_offset(2));
	ph1.AddInstruction(LOAD_SMEM_HOPMEM,
			   RCP_MEM_OFFSET,
			   hop_offset(3));

	VLOG(4) << "writing tpp..." << ph1.MaxSize();
	bytes_tpp += ph1.MaxSize();
	/* Set timestamp */
	u64 *ts = ph1.GetAuxData(0);
	*ts = time_nsec();
	write_buffer(sock, ph1.Bytes(), ph1.MaxSize());
	ph1.Delete();
}

void write_phase3_tpp(int sock)
{
	TPP ph3(3 * 8,
		FLAGS_max_hops /* max-hops */,
		1 /* number of instr. */);

	ph3.AddInstruction(STOREC_HOPMEM_HOPMEM_SMEM,
			   hop_offset(0),
			   hop_offset(1),
			   RCP_MEM_OFFSET);
	/*
	ph3.AddInstruction(STORE_HOPMEM_SMEM,
			   hop_offset(1),
			   RCP_MEM_OFFSET);
	*/

	for (int i = 0; i < FLAGS_max_hops; i++) {
		VLOG(2) << Sprintf(" hop %d: condition prev %llu", i, rcp_link[i].prev_rate);
		ph3.SetHopMem(i, 0, rcp_link[i].prev_rate);
		ph3.SetHopMem(i, 1, (u64)(rcp_link[i].rate * 1e6));
		ph3.SetHopMem(i, 2, 0x6666666666666666LLU);
	}

	VLOG(4) << "writing tpp phase 3..." << ph3.MaxSize();
	bytes_tpp += ph3.MaxSize();
	write_buffer(sock, ph3.Bytes(), ph3.MaxSize());
	ph3.Delete();
}

void loop(int sock) {
	struct sockaddr_in addr;
	u64 last_usec;
	u32 bytes;

	bzero(&addr, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_port = htons(FLAGS_port);
	inet_aton(FLAGS_dest.c_str(), &addr.sin_addr);

	connect(sock, (struct sockaddr *)&addr, sizeof(addr));
	last_usec = time_usec();

	Timer phase1;
	Timer phase3;

	u64 last_ph1_sent = 0;
	u64 last_ph3_sent = 0;
	u64 sent = 0;

	while (true) {
		u64 now;
		usleep(FLAGS_sleep_usec);
		now = time_usec();
		bytes = (now - last_usec) * FLAGS_rate_mbps / 8;
		bytes = min(bytes, 100000U);
		last_usec = now;
		bytes_data += bytes;
		if (bytes >= 1000) {
			/* Have a minimum burst size to write to network. */
			write_total(sock, bytes);
			sent += bytes;
		}

		if (FLAGS_disable_rcp)
			continue;

		if ((sent - last_ph1_sent >= FLAGS_send_bytes) and phase1.elapsed_msec(FLAGS_phase1_interval)) {
			VLOG(3) << "Writing phase 1";
			write_phase1_tpp(sock);
			last_ph1_sent = sent;
		}

		if ((sent - last_ph3_sent >= FLAGS_send_bytes) and FLAGS_commit_rcp[0] == 't' and phase3.elapsed_msec(FLAGS_phase3_interval)) {
			write_phase3_tpp(sock);
			last_ph3_sent = sent;
		}

		read_stuff(sock);
	}
}

int main(int argc, char *argv[])
{
	int optval;
	google::InitGoogleLogging(argv[0]);
	google::ParseCommandLineFlags(&argc, &argv, true);
	FLAGS_logtostderr = true;

	int sock = socket(AF_INET, SOCK_DGRAM, 0);
	CHECK(sock >= 0) << "Couldn't create socket";
	// CHECK(fcntl(sock, F_SETFL, O_NONBLOCK) == 0) << "Couldn't set non blocking on socket";
	CHECK(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) == 0) << "Error setting reuse address";

	for (int i = 0; i < MAX_HOPS; i++)
		rcp_link[i].set_capacity(FLAGS_rcp_capacity_mbps);

	loop(sock);

	return 0;
}
