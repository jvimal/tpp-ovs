#ifndef __COMMON_H__
#define __COMMON_H__

#include <stdarg.h>

#define NSEC_PER_SEC (1000000000LLU)

u64 time_nsec();

struct Timer {
	u64 now;
	u64 last;
	bool auto_reset;

	Timer() {
		now = time_nsec();
		last = now;
		auto_reset = true;
	}

	bool elapsed_nsec(u64 dt_nsec) {
		now = time_nsec();
		if (now >= last + dt_nsec) {
			last = now;
			return true;
		}
		return false;
	}

	bool elapsed_usec(u64 dt_usec) {
		return elapsed_nsec(dt_usec * 1000);
	}

	bool elapsed_msec(u64 dt_msec) {
		return elapsed_nsec(dt_msec * 1000LLU * 1000LLU);
	}

	bool elapsed_sec(u64 dt_sec) {
		return elapsed_nsec(dt_sec * NSEC_PER_SEC);
	}

	bool reset() {
		last = now = time_nsec();
	}
};

string Sprintf(const char *fmt, ...) {
        char buff[1024];
        va_list(ap);
        va_start(ap, fmt);
        vsprintf(buff, fmt, ap);
        va_end(ap);
        return string(buff);
}

#endif /* __COMMON_H__ */
