#ifndef __TPP_H__
#define __TPP_H__

#ifndef DMON_MAGIC
#include "dmon2_lib.c"
#endif

#define RCP_MEM_OFFSET (10)
#ifndef DMON_REPLY
#define DMON_REPLY (0xfaceb00c)
#endif

#include <cassert>
#include <gflags/gflags.h>

using namespace std;
#define MAX_TPP_SIZE (512)
#define MAX_HOPS (5)
#define MAX_AUX_DATA (2)
#define AUX_DATA_SIZE (sizeof(u64))

DEFINE_int32(max_hops, MAX_HOPS, "max hops");

struct TPP {
	u8 *packet;
	struct control_regs *cr;
	u64 *hop_mem;
	u64 preamble;
	u32 hop_mem_size;
	bool alloc;
	u8 *ins_ptr;

	TPP(u8 *packet) {
		this->packet = packet;
		cr = (struct control_regs *)(packet);

		if (Valid())
			Parse();

		alloc = false;
	}

	TPP(int hop_mem_size_bytes, int max_hops, int num_instructions) {
		packet = new u8[MAX_TPP_SIZE];
		alloc = true;

		memset(packet, 0, MAX_TPP_SIZE);
		cr = (struct control_regs *)packet;
		auto size = dmon_prepare_buff(packet, hop_mem_size_bytes,
					      max_hops * hop_mem_size_bytes,
					      num_instructions,
					      0, 0);
		ins_ptr = packet + size;
		hop_mem = (u64 *)HopMem();

		auto L = sizeof(*cr) \
			+ cr->num_instructions * DMON_INSTRUCTION_SIZE_BYTES \
			+ cr->preamble_present * sizeof(u64);
		assert((u8 *)(hop_mem) - packet == L);
	}

	int NumInstructions() {
		return cr->num_instructions;
	}

	int NumHops() {
		return cr->hop_num;
	}

	int NumDataPerHop() {
		return cr->hop_mem_size;
	}

	u64 ReadHopdata(int hop, int mem) {
		return hop_mem[hop * cr->hop_mem_size + mem];
	}

	u64 *GetAuxData(int id) {
		static u64 error;
		if (Valid() and id < MAX_AUX_DATA) {
			u64 *ret = (u64 *)(packet + LastSize());
			return &ret[id];
		}
		/* Fail gracefully */
		return &error;
	}

	void AddInstruction(struct dmon_instruction ins) {
		dmon_encode_instruction(ins_ptr, ins);
		ins_ptr += DMON_INSTRUCTION_SIZE_BYTES;
	}

	void AddInstruction(int opcode, u64 op0, u64 op1=0, u64 op2=0) {
		auto ins = dmon_create_instruction(opcode, op0, op1, op2);
		AddInstruction(ins);
	}

	u8 *Bytes() {
		return packet;
	}

	void Delete() {
		if (alloc) {
			free(packet);
			alloc = false;
		}
	}

	~TPP() {
		Delete();
	}

	bool Valid() {
		return cr && (cr->magic == DMON_MAGIC || cr->magic == DMON_REPLY) && cr->hop_num >= 0;
	}

	bool Reply() {
		return cr && cr->magic == DMON_REPLY && cr->hop_num > 0;
	}

	u64 *HopMem() {
		u64 packet_mem_offset = sizeof(struct control_regs);
		packet_mem_offset += cr->preamble_present * sizeof(u64);
		packet_mem_offset += cr->num_instructions * DMON_INSTRUCTION_SIZE_BYTES;
		return (u64 *)(packet + packet_mem_offset);
	}

	u64 *End() {
		return (u64 *)(packet + LastSize());
	}

	void SetHopMem(int hop, int i, u64 val) {
		hop_mem[hop * cr->hop_mem_size + i] = val;
	}

	void Parse() {
		u8 *temp = packet;
		hop_mem = HopMem();

		hop_mem_size = cr->hop_mem_size * DMON_HOP_MEM_MULTIPLE;
		temp += sizeof(struct control_regs);

		if (cr->preamble_present) {
			preamble = *(u64 *)packet;
			temp += sizeof(u64);
		}
	}

	size_t Size() {
		if (!Valid())
			return -1;

		return sizeof(*cr) + (cr->preamble_present * sizeof(u64)) + \
			cr->num_instructions * DMON_INSTRUCTION_SIZE_BYTES + \
			cr->hop_num * cr->hop_mem_size * DMON_HOP_MEM_MULTIPLE;
	}

	size_t LastSize() {
		return sizeof(*cr) + (cr->preamble_present * sizeof(u64)) + \
			cr->num_instructions * DMON_INSTRUCTION_SIZE_BYTES + \
			MAX_HOPS * cr->hop_mem_size * DMON_HOP_MEM_MULTIPLE;
	}

	size_t MaxSize() {
		return LastSize() + MAX_AUX_DATA * AUX_DATA_SIZE;
	}

	void Dump() {
		int sz = MaxSize();
		for (int i = 0; i < sz; i++) {
			printf("%02x ", (int)packet[i]);
			if (i % 8 == 7) {
				printf(" ");
			}
			if (i % 16 == 15) {
				printf("\n");
			}
			fflush(stdout);
		}
		printf("------------\n");
	}
};

#endif /* __TPP_H__ */
