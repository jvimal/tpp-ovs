#!/bin/bash

KVER=$(uname -r | cut -d. -f 1-2 | sed s/\\./_/g)
sudo rmmod sch_tbf
make

rm -f dmon*
rm -f sch_tbf.c

ln -s ../conga/dmon* .
ln -s sch_tbf_$KVER.c sch_tbf.c

# Note: this module doesn't compile on Linux 3.13.  Working on it (a small
# change to psched_precompute function is needed).

sudo insmod ./sch_tbf.ko enable_tpp=1 enable_limit=0 sample_freq=10