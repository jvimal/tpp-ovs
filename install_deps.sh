#!/bin/bash

sudo apt-get update

sudo apt-get -y install cmake

# Install clang++.  For some reason, the code doesn't compile with g++-4.8/4.7/4.5
sudo apt-get -y install clang-3.5

# Install gflags first
if [ ! -f v2.1.1.tar.gz ]; then
    rm -rf gflags-2.1.1
    wget http://github.com/schuhschuh/gflags/archive/v2.1.1.tar.gz
    tar xf v2.1.1.tar.gz
    pushd gflags-2.1.1

    # build the .a file
    cmake .
    make && sudo make install

    # build the .so file
    cmake -DBUILD_SHARED_LIBS=ON .
    make && sudo make install
    popd
fi

# Install glog
if [ ! -f glog-0.3.3.tar.gz ]; then
    wget http://google-glog.googlecode.com/files/glog-0.3.3.tar.gz
    rm -rf glog-0.3.3
    tar xf glog-0.3.3.tar.gz
    pushd glog-0.3.3
    ./configure

    # This will throw some errors, but it is OK.  It's a known issue:
    # https://code.google.com/p/google-glog/issues/detail?id=203

    make >/dev/null 2>&1
    # Install the libs
    sudo make install-exec;

    # Install the headers
    sudo make install-data;
    popd
fi

# Install the Python dependencies
sudo apt-get install python-matplotlib python-pandas python-statsmodels
sudo easy_install argparse termcolor
sudo apt-get install python-pcapy

sudo apt-get install ethstats

# Install java and floodlight
sudo apt-get install openjdk-7-jdk ant

pushd ~
git clone git://github.com/floodlight/floodlight.git
cd floodlight
ant
test -f target/floodlight.jar
popd
