#ifndef __DMON2_PACKET_H__
#define __DMON2_PACKET_H__

/* Same in host and network byte order. */
#define ETH_P_TPP (0x6666)
#define TPP_MIN_PACKET_SIZE_BYTES (128)

#include "dmon2.h"

struct tpp_header {
	struct control_regs regs;
	u8 packet_mem[0];
};

static u32 tpp_packet_mem_size(struct tpp_header *tph) {
	return tph->regs.packet_mem_size * DMON_PACKET_MEM_MULTIPLE;
}

static u32 tpp_instructions_size(struct tpp_header *tph) {
	return tph->regs.num_instructions * DMON_INSTRUCTION_SIZE_BYTES;
}

static u32 tpp_header_size(struct tpp_header *tph) {
	return sizeof(struct tpp_header) + (tph->regs.preamble_present ? sizeof(u64) : 0);
}

static u32 tpp_size(struct tpp_header *tph) {
	return tpp_header_size(tph) + tpp_instructions_size(tph) + tpp_packet_mem_size(tph);
}

static u8 *tpp_packet_mem(struct tpp_header *tph) {
	return (u8 *)(tph) + sizeof(struct tpp_header) \
		+ (tph->regs.preamble_present ? sizeof(u64) : 0);
}

static int tpp_is_valid(struct tpp_header *tph) {
	return tph->regs.magic == DMON_MAGIC;
}

#endif /* __DMON2_PACKET_H__ */
