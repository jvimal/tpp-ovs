#include <cstdio>
#include <string>
#include <random>
using namespace std;

#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netinet/in.h>
#include <errno.h>
#include <unistd.h>
#include <sys/unistd.h>
#include <sys/fcntl.h>
#include <time.h>
#include <glog/logging.h>
#include <gflags/gflags.h>
#include <arpa/inet.h>
#include <sys/ioctl.h>

#include "dmon2_lib.c"
#include "tpp.cpp"
#include "common.h"

#define NSEC_PER_SEC (1000000000LLU)
#define MAX_PATHS (2)

DEFINE_string(dest, "10.0.0.1", "Destination to connect to");
DEFINE_int32(port, 8001, "Starting dest ports to send data to");
DEFINE_int32(num_paths, 2, "Number of paths");
DEFINE_int32(sleep_usec, 200, "Burst to send in one shot");
DEFINE_double(rate_mbps, 95, "rate in Mb/s");
DEFINE_double(quantisation, 10, "quantisation in Mbps");
DEFINE_double(gain, 0.1, "gain for ewma for congestion");
DEFINE_double(control_interval, 5, "Control interval in msec");
DEFINE_int32(print_interval, 1000, "Print interval in msec");
DEFINE_string(aggregate, "max", "Aggregation function (max/sum)");
DEFINE_int32(mtu, 1500, "MTU");

double congestion[MAX_PATHS];
double last_tx_bytes[MAX_PATHS][MAX_HOPS];
/* The last time (in msec) when congestion was updated */
double last_update_msec[MAX_PATHS];
double last_txupdate_usec[MAX_PATHS][MAX_HOPS];
struct sockaddr_in addr[MAX_PATHS];
int bytes_tpp;
int bytes_data;
void write_query_tpp(int sock);

double Random() {
	static random_device rd;
	static mt19937 gen(rd());
	static uniform_real_distribution<double> dist(1, 2);
	return dist(gen);
}

u64 hop_offset(int offset) {
	return offset;
}

u64 switch_mem(int addr) {
	return addr;
}

u64 time_nsec() {
        struct timespec t;
        clock_gettime(CLOCK_MONOTONIC, &t);
        return t.tv_sec * NSEC_PER_SEC + t.tv_nsec;
}

u64 time_usec() {
	return time_nsec() / 1000;
}

void UpdateCongestion(int path, double cong) {
	static double g = FLAGS_gain;
	double now = time_nsec()/1e6;
	double last = last_update_msec[path];
	last_update_msec[path] = now;
	int num_intervals = (now - last)/FLAGS_control_interval;
	num_intervals = min(num_intervals, 10);
	while (num_intervals--) {
		congestion[path] *= (1.0 - g);
	}
	congestion[path] += g * cong;
}

void write_buffer_path(int sock, int path, u8 *buff, size_t num) {
	struct sockaddr_in a = addr[path];
	while (num > 0) {
		auto written = sendto(sock, buff, num, 0, (struct sockaddr *)&a, sizeof(struct sockaddr_in));
		if (written > 0) {
			num -= written;
			buff += written;
		} else {
			perror("sendto()");
			break;
		}
	}
}

void write_buffer(int sock, u8 *buff, size_t num) {
	while (num > 0) {
		auto written = write(sock, buff, num);
		if (written > 0) {
			num -= written;
			buff += written;
		} else {
			perror("write()");
			break;
		}
	}
}

int pick_path_random() {
	double least = 1e12;
	double r = Random();
	double norm = 0.0;
	double sum = 0;
	for (int p = 0; p < FLAGS_num_paths; p++) {
		norm += congestion[p];
	}
	/* Pick the least loaded path */
	for (int p = 0; p < FLAGS_num_paths; p++) {
		double cp = 1.0 - congestion[p]/norm;
		sum += cp;
		if (sum > r) {
			return p;
		}
	}
	return 0;
}

int pick_path() {
	int path = 0;
	int C = 10000;
	int X = rand() % 100 == 0;
	for (int p = 0; p < FLAGS_num_paths; p++) {
		int Cp = (int)(congestion[p]/FLAGS_quantisation);
		if (Cp < C) {
			C = Cp;
			path = p;
		}
	}
	return path;
}

int get_path(struct sockaddr_in &addr) {
	return htons(addr.sin_port) - FLAGS_port;
}

void write_total(int sock, int num_bytes) {
	static const int write_size = 1400;
	static u8 buff[write_size] = {'x'};
	static int header_overhead = 8 + 20 + 14;

	int num_packets = (FLAGS_mtu - 1 + num_bytes) / FLAGS_mtu;
	num_bytes -= num_packets * header_overhead;
	int path = pick_path();
	VLOG(3) << "Total write size: " << num_bytes;
	while (num_bytes > 0) {
		auto towrite = min(num_bytes, write_size);
		write_buffer_path(sock, path, buff, towrite);
		num_bytes -= min(num_bytes, towrite);
	}
}

double aggregate(vector<double> &cong) {
	double ret = 0;
	if (FLAGS_aggregate == "max") {
		for (auto c: cong) {
			ret = max(ret, c);
		}
	} else if (FLAGS_aggregate == "sum") {
		for (auto c: cong) {
			ret += c;
		}
	}
	return ret;
}

string cong_summary() {
	string ret = "";
	for (int i = 0; i < FLAGS_num_paths; i++) {
		int Ci = (int)(congestion[i]/FLAGS_quantisation); /* Quantisation */
		ret += Sprintf("%.2f(%d),", congestion[i], Ci);
	}
	return ret;
}

Timer printphase;
Timer controlphase;

void read_stuff(int sock) {
	static const int read_size = 4096;
	static u8 buff[read_size];
	vector<double> cong_hops;
	static int last_send_bytes = 0;

	while (true) {
		int count;
		ioctl(sock, FIONREAD, &count);
		if (count == 0)
			break;

		VLOG(4) << "Reading...";
		struct sockaddr_in addr;
		socklen_t addrlen = sizeof(addr);
		auto num_read = recvfrom(sock, buff, read_size, 0,
				(struct sockaddr *)&addr, &addrlen);

		/* Process the message */
		TPP tpp(buff);
		if (!tpp.Valid())
			continue;

		/* Which path was the TPP sent out of? */
		int path = get_path(addr);
		bytes_tpp += tpp.MaxSize();
		if (tpp.Reply()) {
			if (tpp.NumInstructions() != 3)
				continue;

			cong_hops.clear();
			bool ok = printphase.elapsed_msec(FLAGS_print_interval);

			for (int i = 1; i < tpp.NumHops()-1; i++) {
				if (ok) {
					for (int j = 0; j < tpp.NumDataPerHop(); j++) {
						VLOG(2) << " hop[" << i << "][" << j << "] = " << tpp.ReadHopdata(i, j);
						u64 data = tpp.ReadHopdata(i, j);
						if (j == 1) {
							char *buff = (char *)&data;
							VLOG(2) << "   " << buff;
						}
					}
				}

				auto util = tpp.ReadHopdata(i, 2);
				auto tx_bytes = tpp.ReadHopdata(i, 0);
				double now = time_usec();
				double dt_usec = now - last_txupdate_usec[path][i];
				cong_hops.push_back((tx_bytes - last_tx_bytes[path][i]) * 8.0/dt_usec);
				last_tx_bytes[path][i] = tx_bytes;
				last_txupdate_usec[path][i] = now;
			}

			auto cong = aggregate(cong_hops);
			/* EWMA */
			UpdateCongestion(path, cong);

			if (ok) {
				float overhead = (bytes_tpp) * 100.0;
				overhead /= (0.0001 + bytes_tpp + bytes_data);
				VLOG(1) << Sprintf("bytes_tpp: %d, bytes_data: %d, overhead: %.3f%%\n",
							bytes_tpp, bytes_data, overhead);
				float send_rate = (bytes_tpp + bytes_data - last_send_bytes) * 8.0;
				send_rate /= (FLAGS_print_interval * 1000.0);
				last_send_bytes = bytes_tpp + bytes_data;
				VLOG(1) << Sprintf("avg sending rate: %.3fMb/s, cong: %s, hops: %d\n", send_rate, cong_summary().c_str(), cong_hops.size());
			}
		} else {
			/* Valid TPP, so reflect it */
			tpp.cr->magic = DMON_REPLY;
			write_buffer_path(sock, 0, tpp.packet, tpp.MaxSize());
		}
	}
}

void write_query_tpp(int sock)
{
	TPP ph1(3 * 8,
		FLAGS_max_hops /* max-hops */,
		3 /* number of instr. */);
	ph1.AddInstruction(LOAD_REG_HOPMEM,
			   REG_EGRESS_TX_BYTES,
			   hop_offset(0));
	ph1.AddInstruction(LOAD_REG_HOPMEM,
			   REG_SW_DPID,
			   hop_offset(1));
	ph1.AddInstruction(LOAD_REG_HOPMEM,
			   REG_EGRESS_TX_RATE,
			   hop_offset(2));

	VLOG(4) << "writing tpp..." << ph1.MaxSize();
	bytes_tpp += ph1.MaxSize();
	for (int p = 0; p < FLAGS_num_paths; p++) {
		write_buffer_path(sock, p, ph1.Bytes(), ph1.MaxSize());
	}
	ph1.Delete();
}

void loop(int sock) {
	u64 last_usec;
	u32 bytes;

	for (int i = 0; i < MAX_PATHS; i++) {
		bzero(&addr[i], sizeof(addr[i]));
		addr[i].sin_family = AF_INET;
		addr[i].sin_port = htons(FLAGS_port+i);
		inet_aton(FLAGS_dest.c_str(), &addr[i].sin_addr);
	}

	last_usec = time_usec();

	Timer phase1;
	while (true) {
		u64 now;
		usleep(FLAGS_sleep_usec);
		now = time_usec();
		bytes = (now - last_usec) * FLAGS_rate_mbps / 8;
		bytes = min(bytes, 100 * 1000U);
		if (bytes >= 1000U) {
			last_usec = now;
			bytes_data += bytes;
			write_total(sock, bytes);
		}

		if (phase1.elapsed_msec(FLAGS_control_interval)) {
			VLOG(3) << "Writing control TPP";
			write_query_tpp(sock);
		}

		read_stuff(sock);
	}
}

int main(int argc, char *argv[])
{
	int optval;
	google::InitGoogleLogging(argv[0]);
	gflags::ParseCommandLineFlags(&argc, &argv, true);
	FLAGS_logtostderr = true;

	int sock = socket(AF_INET, SOCK_DGRAM, 0);
	CHECK(sock >= 0) << "Couldn't create socket";
	// CHECK(fcntl(sock, F_SETFL, O_NONBLOCK) == 0) << "Couldn't set non blocking on socket";
	CHECK(setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof(optval)) == 0) << "Error setting reuse address";

	loop(sock);

	return 0;
}
