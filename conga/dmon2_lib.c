
#include "dmon2.h"
#include "dmon2_packet.h"

/* The second version of dmon */

u64 msb_4bits(u8 ptr) {
	return (ptr & 0xf0) >> 4;
}

u64 lsb_4bits(u8 ptr) {
	return (ptr & 0xf);
}

u64 le_16bit(u8 *ptr) {
	u64 ret = ptr[1];
	return (u64)ptr[0] | (ret << 8);
}

void emit_le_16bit(u8 *ptr, u64 val) {
	*ptr++ = val & 0xff; val >>= 8;
	*ptr++ = val & 0xff; val >>= 8;
}

void emit_msb_4bits(u8 *ptr, u64 val) {
	*ptr |= ((val & 0xf) << 4);
}

void emit_lsb_4bits(u8 *ptr, u64 val) {
	*ptr |= (val & 0xf);
}

struct dmon_instruction
dmon_decode_instruction(u8 *ptr, struct control_regs cr) {
	struct dmon_instruction ret;
	ret.size_bytes = DMON_INSTRUCTION_SIZE_BYTES;
	ret.ins = (enum dmon_instruction_type)(*ptr++);

	ret.num_operands = DMON_NUM_OPERANDS[ret.ins];
	switch (ret.ins) {
	case LOAD_REG_HOPMEM:
		ret.operands[0].type = OP_REG;
		ret.operands[0].val = ptr[0];
		ret.operands[1].type = OP_HOPMEM;
		ret.operands[1].val = msb_4bits(ptr[1]);
		break;

	case LOAD_SMEM_HOPMEM:
		ret.operands[0].type = OP_SMEM;
		ret.operands[0].val = le_16bit(ptr); // ptr[0,1]
		ret.operands[1].type = OP_HOPMEM;
		ret.operands[1].val = msb_4bits(ptr[2]);
		break;

	case STORE_HOPMEM_REG:
		ret.operands[0].type = OP_HOPMEM;
		ret.operands[0].val = msb_4bits(ptr[0]);
		ret.operands[1].type = OP_REG;
		ret.operands[1].val = ptr[1];
		break;

	case STORE_HOPMEM_SMEM:
		ret.operands[0].type = OP_HOPMEM;
		ret.operands[0].val = msb_4bits(ptr[0]);
		ret.operands[1].type = OP_SMEM;
		ret.operands[1].val = le_16bit(ptr + 1);
		break;

	case STOREC_HOPMEM_HOPMEM_REG:
		ret.operands[0].type = OP_HOPMEM;
		ret.operands[0].val = msb_4bits(ptr[0]);
		ret.operands[1].type = OP_HOPMEM;
		ret.operands[1].val = lsb_4bits(ptr[0]);
		ret.operands[2].type = OP_REG;
		ret.operands[2].val = ptr[1];
		break;

	case STOREC_HOPMEM_HOPMEM_SMEM:
		ret.operands[0].type = OP_HOPMEM;
		ret.operands[0].val = msb_4bits(ptr[0]);
		ret.operands[1].type = OP_HOPMEM;
		ret.operands[1].val = lsb_4bits(ptr[0]);
		ret.operands[2].type = OP_SMEM;
		ret.operands[2].val = le_16bit(ptr + 1);
		break;

	case BITSET_POS_REG: /* register probably not needed */
		break;

	case BITSET_POS_SMEM:
		ret.operands[0].type = OP_IMM;
		ret.operands[0].val = ptr[0];
		ret.operands[1].type = OP_SMEM;
		ret.operands[1].val = le_16bit(ptr + 1);
		break;

	case BITCLEAR_POS_REG:
		break;

	case BITCLEAR_POS_SMEM:
		break;

	case EXE_IF_REG_MATCHES_PREAMBLE:
		ret.operands[0].type = OP_REG;
		ret.operands[0].val = msb_4bits(ptr[0]);
		break;

	case STOREPUSH_SMEM:
	case STOREPUSH_REG:
	default:
		break;
	}

	return ret;
}

struct dmon_instruction
dmon_create_instruction(int opcode, u64 op0, u64 op1, u64 op2)
{
	struct dmon_instruction ret;
	ret.ins = (enum dmon_instruction_type)opcode;
	ret.num_operands = 0;
	if (op0 != ~0) {
		ret.operands[0].val = op0;
		ret.num_operands += 1;
	}

	if (op1 != ~0) {
		ret.operands[1].val = op1;
		ret.num_operands += 1;
	}

	if (op2 != ~0) {
		ret.operands[2].val = op2;
		ret.num_operands += 1;
	}

	return ret;
}

void
dmon_encode_instruction(u8 *ptr, struct dmon_instruction ins) {
	ptr[0] = ins.ins;
	ptr[1] = 0;
	ptr[2] = 0;
	ptr[3] = 0;
	ptr++;
	switch (ins.ins) {
	case LOAD_REG_HOPMEM:
		ptr[0] = ins.operands[0].val;
		emit_msb_4bits(ptr+1, ins.operands[1].val);
		break;

	case LOAD_SMEM_HOPMEM:
		emit_le_16bit(ptr, ins.operands[0].val);
		emit_msb_4bits(ptr+2, ins.operands[1].val);
		break;

	case STORE_HOPMEM_REG:
		emit_msb_4bits(ptr, ins.operands[0].val);
		ptr[1] = ins.operands[1].val;
		break;

	case STORE_HOPMEM_SMEM:
		emit_msb_4bits(ptr, ins.operands[0].val);
		emit_le_16bit(ptr+1, ins.operands[1].val);
		break;

	case STOREC_HOPMEM_HOPMEM_REG:
		emit_msb_4bits(ptr, ins.operands[0].val);
		emit_lsb_4bits(ptr, ins.operands[1].val);
		ptr[1] = ins.operands[2].val;
		break;

	case STOREC_HOPMEM_HOPMEM_SMEM:
		emit_msb_4bits(ptr, ins.operands[0].val);
		emit_lsb_4bits(ptr, ins.operands[1].val);
		emit_le_16bit(ptr+1, ins.operands[2].val);
		break;

	case BITSET_POS_REG: /* register probably not needed */
		break;

	case BITSET_POS_SMEM:
		ptr[0] = ins.operands[0].val;
		emit_le_16bit(ptr+1, ins.operands[1].val);
		break;

	case BITCLEAR_POS_REG:
		break;

	case BITCLEAR_POS_SMEM:
		break;

	case EXE_IF_REG_MATCHES_PREAMBLE:
		emit_msb_4bits(ptr, ins.operands[0].val);
		break;

	case STOREPUSH_REG:
	case STOREPUSH_SMEM:
	default:
		break;
	}
}

int dmon_execute_instruction(struct dmon_instruction ins,
			     u64 *hop_mem,
			     u32 hop_mem_size,
			     u64 *reg_file,
			     u32 reg_file_size,
			     u64 *smem,
			     u32 smem_size,
			     u64 *emem,
			     u32 emem_size,
			     u64 preamble)
{
	int ret = 0;
	u64 op0 = ins.operands[0].val;
	u64 op1 = ins.operands[1].val;
	u64 op2 = ins.operands[2].val;

	//printk(KERN_INFO "%s (%d): %llu %llu\n", dmon_instruction_str[ins.ins], ins.ins, op0, op1);

	switch (ins.ins) {
	case LOAD_REG_HOPMEM:
		if (op1 < hop_mem_size && op0 < reg_file_size) {
			hop_mem[op1] = reg_file[op0];
		}

		if (op0 > REG_EGRESS_START && op0 < REG_EGRESS_MAX && (op0 - REG_EGRESS_START - 1) < emem_size) {
			emem[op0 - REG_EGRESS_START - 1] = (u64)&hop_mem[op1];
		}

		break;

	case LOAD_SMEM_HOPMEM:
		if (op1 < hop_mem_size && op0 < smem_size)
			hop_mem[op1] = smem[op0];
		break;

	case STORE_HOPMEM_REG:
		if (op1 < reg_file_size && op0 < hop_mem_size)
			reg_file[op1] = hop_mem[op0];
		break;

	case STORE_HOPMEM_SMEM:
		if (op1 < smem_size && op0 < hop_mem_size)
			smem[op1] = hop_mem[op0];
		break;

	case STOREC_HOPMEM_HOPMEM_REG:
		if (op2 < reg_file_size && op0 < hop_mem_size && op1 < hop_mem_size) {
		if (reg_file[op2] == hop_mem[op0]) {
			reg_file[op2] = hop_mem[op1];
		}
		hop_mem[op0] = reg_file[op2];
		}
		break;

	case STOREC_HOPMEM_HOPMEM_SMEM:
//#ifdef printk
//		printk(KERN_INFO "storec-hop-hop-smem %d %d %d (smem[op2]=%llu, hop_mem[op0]=%llu)\n", op0, op1, op2, smem[op2], hop_mem[op0]);
//#endif
		if (op2 < smem_size && op0 < hop_mem_size && op1 < hop_mem_size) {
		if (smem[op2] == hop_mem[op0]) {
			smem[op2] = hop_mem[op1];
		}
		hop_mem[op0] = smem[op2];
		}
		break;

	case BITSET_POS_REG: /* register probably not needed */
		if (op1 < reg_file_size)
			reg_file[op1] |= (1LLU << op0);
		break;

	case BITSET_POS_SMEM:
		if (op1 < smem_size)
			smem[op1] |= (1LLU << op0);
		break;

	case BITCLEAR_POS_REG:
		if (op1 < reg_file_size)
			reg_file[op1] &= ~(1LLU << op0);
		break;

	case BITCLEAR_POS_SMEM:
		if (op1 < smem_size)
			smem[op1] &= ~(1LLU << op0);
		break;

	case EXE_IF_REG_MATCHES_PREAMBLE: {
		u64 mask = preamble >> 32;
		u64 value = preamble & (0xffffffff);
		if (op0 < reg_file_size)
			ret = !((reg_file[op0] & mask) == value);
		break;
	}

	case STOREPUSH_SMEM:
	case STOREPUSH_REG:
	default:
		break;
	}

	return ret;
}

void dmon_execute(u8 *packet, u64 *reg_file, u32 reg_file_size,
		  u64 *smem, u32 smem_size,
		  u64 *emem, u32 emem_size)
{
	struct dmon_instruction ins;
	u64 *hop_mem;
	u8 *ins_ptr;
	u64 preamble = 0;
	int i;
	int ret;
	int packet_mem_offset = 0;
	struct control_regs *cr = (struct control_regs *)(packet);
	int hop_mem_size = 0;

	if (cr->magic != DMON_MAGIC)
		return;

	packet_mem_offset = sizeof(struct control_regs);
	packet_mem_offset += cr->preamble_present * sizeof(u64);
	packet_mem_offset += cr->num_instructions * DMON_INSTRUCTION_SIZE_BYTES;
	hop_mem = (u64 *)(packet					\
			  + (packet_mem_offset)				\
			  + (cr->hop_num * cr->hop_mem_size * DMON_HOP_MEM_MULTIPLE));
	hop_mem_size = cr->hop_mem_size * DMON_HOP_MEM_MULTIPLE;

	packet += sizeof(struct control_regs);

	if (cr->preamble_present) {
		preamble = *(u64 *)packet;
		packet += sizeof(u64);
	}

	ins_ptr = packet;

	// printk(KERN_INFO "number of instructions: %d\n", cr->num_instructions);

	for (i = 0; i < cr->num_instructions; i++) {
		ins = dmon_decode_instruction(ins_ptr, *cr);
		ret = dmon_execute_instruction(ins, hop_mem, hop_mem_size,
					       reg_file, reg_file_size,
					       smem, smem_size, emem, emem_size,
					       preamble);
		if (ret)
			break;
		ins_ptr += ins.size_bytes;
	}

	cr->hop_num += 1;
}

int dmon_prepare_buff(u8 *pkt,
		      int hop_mem_size_bytes,
		      int packet_mem_size_bytes,
		      int num_instructions,
		      int preamble_present,
		      u64 preamble)
{
	struct control_regs *cr = (struct control_regs *)pkt;
	int size = 0;
	cr->magic = DMON_MAGIC;
	cr->is_programmed = 1;
	cr->hop_mem_size = hop_mem_size_bytes / DMON_HOP_MEM_MULTIPLE;
	cr->hop_num = 0;
	cr->packet_mem_size = packet_mem_size_bytes / DMON_PACKET_MEM_MULTIPLE;
	cr->num_instructions = num_instructions;
	cr->preamble_present = preamble_present;
	cr->reserved = 0;
	pkt += sizeof(*cr);
	size += sizeof(*cr);

	if (cr->preamble_present) {
		*(u64 *)pkt = preamble;
		size += sizeof(u64);
	}

	return size;
}

#ifdef test_dmon
#include <string.h>
#include <stdio.h>

void dmon_print_instruction(struct dmon_instruction ins)
{
	int i;
	printf(" (%x) %s ", ins.ins, dmon_instruction_str[ins.ins]);
	for (i = 0; i < ins.num_operands; i++) {
		printf("%s:%llu, ",
		       operand_type_str[ins.operands[i].type],
		       ins.operands[i].val);
	}
	printf("\n");
}

void dump_bytes(u8 *ptr, int size) {
	int i = 0;
	for (i = 0; i < size; i++) {
		printf("%02x ", ptr[i]);
		if ((i+1) % 8 == 0) printf("   ");
		if ((i+1) % 16 == 0) printf("\n");
	}
	printf("\n");
}

void dump_prog(u8 *ptr) {
	struct control_regs *cr = (struct control_regs *)ptr;
	u64 *preamble = (u64*)(ptr + sizeof(*cr));
	u8 *ins_ptr;
	struct dmon_instruction ins;
	int i;

	printf("Control registers:\n");
	printf("  magic: %x\n", cr->magic);
	if (cr->magic != DMON_MAGIC)
		return;

	printf("  programmed: %d\n", cr->is_programmed);
	printf("  hop mem size: %d bytes\n", cr->hop_mem_size * DMON_HOP_MEM_MULTIPLE);
	printf("  hop num: %d\n", cr->hop_num);
	printf("  packet mem size: %d bytes\n", cr->packet_mem_size * DMON_PACKET_MEM_MULTIPLE);
	printf("  num instructions: %d\n", cr->num_instructions);
	printf("  preamble present: %d\n", cr->preamble_present);

	if (cr->preamble_present) {
		printf("Preamble: %llu (mask: %llu, value: %llu)\n",
		       *preamble, (*preamble >> 32), (*preamble & 0xffffffff));
	}

	ins_ptr = (ptr + sizeof(*cr) + (cr->preamble_present * sizeof(u64)));

	for (i = 0; i < cr->num_instructions; i++) {
		ins = dmon_decode_instruction(ins_ptr, *cr);
		dmon_print_instruction(ins);
		ins_ptr += ins.size_bytes;
	}
}

void dump_hopdata(u8 *ptr) {
	struct control_regs *cr = (struct control_regs *)ptr;
	int i, j;
	int packet_mem_offset = 0;
	u64 *hop_mem;

	if (cr->magic != DMON_MAGIC)
		return;

	packet_mem_offset = sizeof(struct control_regs);
	packet_mem_offset += cr->preamble_present * sizeof(u64);
	packet_mem_offset += cr->num_instructions * DMON_INSTRUCTION_SIZE_BYTES;

	for (i = 0; i < cr->hop_num; i++) {
		hop_mem = (u64 *)(ptr					\
				  + (packet_mem_offset)			\
				  + (i * cr->hop_mem_size * DMON_HOP_MEM_MULTIPLE));
		for (j = 0; j < cr->hop_mem_size; j++) {
			printf(" hop[%d][%d] = %llu (%llx)\n", i, j, hop_mem[j], hop_mem[j]);
		}
	}
}

u64 hop_offset(int offset) {
	return offset;
}

u64 switch_mem(int addr) {
	return addr;
}

int main() {
	u8 buff[1024];
	u8 *packet = buff;
	u8 *ins_ptr;
	int per_hop_bytes = 6 * 8;
	int max_hops = 5;
	int num_instructions = 4;
	int preamble_present = 0;
	u64 preamble = 0;
	int size;

	bzero(buff, sizeof(buff));
	size = dmon_prepare_buff(buff, per_hop_bytes, max_hops * per_hop_bytes,
				 num_instructions, preamble_present, preamble);

	ins_ptr = packet + size;
	dmon_encode_instruction(ins_ptr,
				dmon_create_instruction(LOAD_REG_HOPMEM,
							REG_LINK_UTIL,
							hop_offset(0),
							~0));
	ins_ptr += DMON_INSTRUCTION_SIZE_BYTES;

	dmon_encode_instruction(ins_ptr,
				dmon_create_instruction(LOAD_REG_HOPMEM,
							REG_QUEUE_ENQUEUED,
							hop_offset(1),
							~0));
	ins_ptr += DMON_INSTRUCTION_SIZE_BYTES;

	dmon_encode_instruction(ins_ptr,
				dmon_create_instruction(STOREC_HOPMEM_HOPMEM_SMEM,
							hop_offset(2),
							hop_offset(3),
							switch_mem(0)));
	ins_ptr += DMON_INSTRUCTION_SIZE_BYTES;

	dmon_encode_instruction(ins_ptr,
				dmon_create_instruction(STOREC_HOPMEM_HOPMEM_SMEM,
							hop_offset(4),
							hop_offset(5),
							switch_mem(1)));
	ins_ptr += DMON_INSTRUCTION_SIZE_BYTES;

	dump_bytes(packet, 64);
	dump_prog(packet);

	{
		u64 reg_file[100];
		u64 smem[1000];

		reg_file[5] = 0xfeed;
		reg_file[6] = 0xb00c;
		reg_file[REG_PACKET_HOPNUM] = 1;
		reg_file[REG_PACKET_INPORT] = 1;
		reg_file[REG_PACKET_OUTPORT0] = 1;
		dmon_execute(packet, reg_file, smem);

		dump_bytes(packet, 128);
		dump_hopdata(packet);
	}
}
#endif
