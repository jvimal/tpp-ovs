
#ifndef __DMON2_H__
#define __DMON2_H__

#if !defined(u32)
typedef unsigned char u8;
typedef unsigned int u32;
typedef unsigned short u16;
typedef unsigned long long u64;
#endif

#include "dmon2_consts.h"

enum dmon_instruction_type {
	LOAD_REG_HOPMEM,
	LOAD_SMEM_HOPMEM,

	STORE_HOPMEM_REG,
	STORE_HOPMEM_SMEM,

	STOREC_HOPMEM_HOPMEM_REG,
	STOREC_HOPMEM_HOPMEM_SMEM,

	BITSET_POS_REG, /* register probably not needed */
	BITSET_POS_SMEM,

	BITCLEAR_POS_REG,
	BITCLEAR_POS_SMEM,

	STOREPUSH_REG,
	STOREPUSH_SMEM,

	EXE_IF_REG_MATCHES_PREAMBLE,
};

#define S(E) [E] = #E,
#define N(E,n) [E] = n,

static char *dmon_instruction_str[] = {
	S(LOAD_REG_HOPMEM)
	S(LOAD_SMEM_HOPMEM)

	S(STORE_HOPMEM_REG)
	S(STORE_HOPMEM_SMEM)

	S(STOREC_HOPMEM_HOPMEM_REG)
	S(STOREC_HOPMEM_HOPMEM_SMEM)

	S(BITSET_POS_REG)
	S(BITSET_POS_SMEM)

	S(BITCLEAR_POS_REG)
	S(BITCLEAR_POS_SMEM)

	S(STOREPUSH_REG)
	S(STOREPUSH_SMEM)

	S(EXE_IF_REG_MATCHES_PREAMBLE)
};
/*
 * Number of operands per instruction
 */
static int DMON_NUM_OPERANDS[] = {
	N(LOAD_REG_HOPMEM,2)
	N(LOAD_SMEM_HOPMEM,2)

	N(STORE_HOPMEM_REG,2)
	N(STORE_HOPMEM_SMEM,2)

	N(STOREC_HOPMEM_HOPMEM_REG,3)
	N(STOREC_HOPMEM_HOPMEM_SMEM,3)

	N(BITSET_POS_REG,2)
	N(BITSET_POS_SMEM,2)

	N(BITCLEAR_POS_REG,2)
	N(BITCLEAR_POS_SMEM,2)

	N(STOREPUSH_REG,2)
	N(STOREPUSH_SMEM,2)

	N(EXE_IF_REG_MATCHES_PREAMBLE,1)
};

enum operand_type {
	OP_REG,
	OP_SMEM,
	OP_HOPMEM,
	OP_IMM,
};

static char *operand_type_str[] = {
	S(OP_REG)
	S(OP_SMEM)
	S(OP_HOPMEM)
	S(OP_IMM)
};

enum registers {
	REG_SW_DPID = 0,
	REG_SW_FLOWTABLE_VERSION,
	REG_SW_FLAGS,

	REG_LINK_RX,
	REG_LINK_TX,
	REG_LINK_UTIL,

	REG_QUEUE_ENQUEUED,

	REG_PACKET_HOPNUM,
	REG_PACKET_INPORT,

	REG_EGRESS_START,
	REG_EGRESS_PORT,
	REG_EGRESS_QUEUE_ENQUEUED,
	REG_EGRESS_RX_RATE,
	REG_EGRESS_TX_RATE,
	REG_EGRESS_TX_BYTES,
	REG_EGRESS_MAX,

	REG_TSTAMP,
};

enum control_registers {
	CR_IS_PROGRAMED,
	CR_HOP_MEM_SIZE,
	CR_HOP_NUM,
	CR_PACKET_MEM_OFFSET,
	CR_INSTR_OFFSET,
};

#define MAX_OPERANDS (3)
struct dmon_instruction {
	enum dmon_instruction_type ins;
	int num_operands;
	struct dmon_operand {
		enum operand_type type;
		u64 val;
	} operands[MAX_OPERANDS];
	u8 *offset;
	u8 size_bytes;
};

struct control_regs {
	u32 magic;  /* Magic value: only for prototype */
	u8 is_programmed : 1;
	u8 hop_mem_size : 3; /* Multiple of 8 bytes */
	u8 hop_num : 4;
	u8 packet_mem_size : 8; /* Multiple of 4 bytes */
	// u8 instr_offset : 4; /* Multiple of 4 bytes */
	u8 num_instructions : 4;
	u8 preamble_present : 1;
	u8 reserved : 3; /* Reserved */
	u8 reserved2 : 8;
} __attribute__((packed));

#define MIN_DMON_PKT_LEN (64)
#define DMON_MAGIC (0xfaceface)
#define DMON_MAGIC_REPLY (0xfaceb00c)
#define DMON_INSTRUCTION_SIZE_BYTES (4)
#define DMON_PACKET_MEM_MULTIPLE (4)
#define DMON_HOP_MEM_MULTIPLE (8)
#endif /* __DMON2_H__ */
