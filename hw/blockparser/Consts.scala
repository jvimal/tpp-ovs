package Consts
import Chisel._

object Consts {
  val BIT_WIDTH = 32
  val OFFSET_WIDTH = 16
  val FIELD_SIZE_WIDTH = 6
}

object PacketOffset extends UInt {
  def apply(dir: IODirection = null): UInt = {
    val res = new UInt()
    res.create(dir, Consts.OFFSET_WIDTH)
    res
  }
}
