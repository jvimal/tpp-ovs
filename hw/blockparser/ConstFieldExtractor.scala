package ConstFieldExtractor
import Chisel._
import Consts._
import Math._

class ConstFieldExtractor(block_size: Int, field_offset: Int, field_width: Int) extends Module {
  val io = new Bundle {
    /* The current packet block */
    val block = UInt(INPUT, width=Consts.BIT_WIDTH)

    /* The offset into the packet this block belongs */
    val offset = PacketOffset(INPUT)

    /* The output of this parser */
    val field = UInt(OUTPUT, width=field_width)
    val rdy = Bool(OUTPUT)
  }

  /*
   * We generate the extractor as follows.  Given a field_offset and
   * width, we know the [start,end] bits we need to extract at compile
   * time.  Given this, we can intersect with [block,
   * (block+1)*block_size] and determine if we need to collect these
   * bits.  If so, collect them and finally concat all of them.
   */

  io.rdy := Bool(false)

  when (io.offset === UInt(0)) {
    io.rdy := Bool(false)
  }

  var block_start = 0
  val field_start = field_offset
  val field_end = field_start + field_width - 1
  var block_index = 0
  var bit_curr = 0

  /* Create as many registers as needed (number of intersecting blocks) */
  var N = field_end/block_size - field_start/block_size + 1
  /* The size of the registers will be inferred from the io.block(ss,ee) assignment */
  val output_bits = Vec.fill(N) { Reg(next=UInt(0)) }
  var which_part = 0

  while (block_start <= field_end) {
    val block_end = block_start + block_size - 1
    val s = max(field_start, block_start)
    val e = min(field_end, block_end)
    val bits = e-s+1
    if (bits > 0) {
      /* We intersect and we need a register to hold this */
      when (io.offset === UInt(block_start)) {
        val ss = block_end - s;
        val ee = block_end - e;
        /* This will help infer the register in output_bits sizes */
        output_bits(which_part) := io.block(ss, ee)
        println("Will extract (%d,%d), i.e. bits(%d,%d) -> %d at block %d".format(s, e, ss, ee, bits, block_index))
      }

      bit_curr += bits
      which_part += 1
    }
    block_index += 1
    block_start += block_size
  }

  /* Concatenate all bits */
  io.field := output_bits.reduce(Cat(_, _))

  /* And finally... */
  when (io.offset > UInt(field_end)) {
    io.rdy := Bool(true)
  }
}

class ConstFieldExtractorTests(c: ConstFieldExtractor, w: Int) extends Tester(c) {
  var good = true
  val packet_data = "ffffeeeeffff" + "feeffeefffff" + "8100" + "8000" + "45000098118a000001111b0d0a1e92a6efff" + "fffaf7e6076c00846b664d2d53454152"
  var offset = 0
  var count = 0

  while (count < 4 && offset + w < 4 * packet_data.size) {
    count += 1
    val block = packet_data.substring(offset/4, (offset+w)/4)
    printf("\nOffset = %d, inp = %s\n", offset, block)
    poke(c.io.block,  BigInt(block, 16))
    poke(c.io.offset, offset)
    step(1)
    peek(c.io.field)
    peek(c.io.rdy)
    offset += w
  }

  true
}

object ConstFieldExtractor {
  def main(args: Array[String]): Unit = {
    var block_width = Consts.BIT_WIDTH
    chiselMainTest(args.slice(1, args.length),
      () => Module(new ConstFieldExtractor(block_width, 48, 48))) {
        c => new ConstFieldExtractorTests(c, block_width)
      }
  }
}
