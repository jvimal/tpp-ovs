package FieldExtractor
import Chisel._
import Consts._

class FieldExtractor(block_size: Int, field_width: Int) extends Module {
  val io = new Bundle {
    /* The current packet block */
    val block = UInt(INPUT, width=Consts.BIT_WIDTH)
    val field_offset = PacketOffset(INPUT)

    /* The offset into the packet this block belongs */
    val offset = PacketOffset(INPUT)

    /* The output of this parser */
    val field = UInt(OUTPUT, width=field_width)
    val rdy = Bool(OUTPUT)
  }

  val start = io.field_offset
  val end = io.field_offset + UInt(field_width - 1, width=Consts.OFFSET_WIDTH)
  val block_end = io.offset + UInt(block_size - 1)
  val out = Reg(UInt(width=field_width))

  def min(a: UInt, b: UInt) = {
    Mux(a < b, a, b)
  }
  def max(a: UInt, b: UInt) = {
    Mux(a < b, b, a)
  }

  val msbit = UInt(io.block.getWidth)

  def genmask(width: UInt) = {
    ((UInt(1) << width) - UInt(1))(field_width-1, 0)
  }
  def getbits(startbit: UInt, width: UInt) = {
    /* get width bits from startbit.  MSB is bit 0. */
    val tmp = io.block >> (msbit - startbit - width)
    tmp & genmask(width)
  }

  io.rdy := Bool(false)
  io.field <> out

  /* Initialization */
  when (io.offset === UInt(0)) {
    io.rdy := Bool(false)
    out := UInt(0)
  }

  val s = max(io.offset, start)
  val e = min(block_end, end)

  when (s <= e) {
    /* max and min will intersect [start,end] with [offset,block_end] */
    val L = (e - s + UInt(1))(Consts.FIELD_SIZE_WIDTH-1, 0)
    val bits = getbits(s - UInt(io.offset), L)
    val shifted = (out << L)(field_width-1, 0)
    out := shifted + bits
    printf("Start: %x, End: %x, Out: %x, Block: %x\n", s, e, bits, io.block)
  }

  /* And finally... */
  when (io.offset > end) {
    io.rdy := Bool(true)
  }
}

class FieldExtractorTests(c: FieldExtractor, w: Int) extends Tester(c) {
  var good = true
  val packet_data = "ffffeeeeffff" + "feeffeefffff" + "8100" + "8000" + "45000098118a000001111b0d0a1e92a6efff" + "fffaf7e6076c00846b664d2d53454152"
  var offset = 0
  var count = 0

  while (count < 4 && offset + w < 4 * packet_data.size) {
    count += 1
    val block = packet_data.substring(offset/4, (offset+w)/4)
    printf("\nOffset = %d, inp = %s\n", offset, block)
    poke(c.io.block,  BigInt(block, 16))
    poke(c.io.offset, offset)
    step(1)
    peek(c.io.field)
    peek(c.io.rdy)
    offset += w
  }

  true
}

/*
object FieldExtractor {
  def main(args: Array[String]): Unit = {
    var block_width = Consts.BIT_WIDTH
    chiselMainTest(args.slice(1, args.length),
      () => Module(new FieldExtractor(block_width, UInt(48, width=Consts.FIELD_SIZE_WIDTH), 48))) {
        c => new FieldExtractorTests(c, block_width)
      }
  }
}
*/
