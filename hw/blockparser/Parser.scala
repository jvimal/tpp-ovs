package Parser
import Chisel._
import Consts._
import FieldExtractor._
import ConstFieldExtractor._

class Parser(block_size: Int, fields: Array[(Int, Int)]) extends Module {
  val io = new Bundle {
    val block = UInt(INPUT, width=Consts.BIT_WIDTH)
    val offset = PacketOffset(INPUT)
    val start_offset = PacketOffset(INPUT)
    val data_offset = PacketOffset(OUTPUT)
    val out_fields = Vec(fields.map{ case(offset,width) => UInt(OUTPUT, width=width) })
    val rdys = Vec(fields.map{ _ => Bool(OUTPUT) })
  }

  /* For each (offset,width) in fields, generate a field extractor at
   * start_offset+offset */
  var count = 0

  /* Use this for variable field offset
  val foff = Vec(fields.map{ case(offset,width) => io.start_offset + UInt(offset, width=width) })
   */

  fields.foreach { case (offset,width) =>
    var f = Module(new ConstFieldExtractor(block_size, offset, width))
    f.io.block <> io.block
    f.io.offset <> io.offset
    f.io.field <> io.out_fields(count)
    /* use this for variable field offset
    f.io.field_offset := foff(count)
     */
    f.io.rdy <> io.rdys(count)
    count += 1
  }
}

class ParserTests(c: Parser) extends Tester(c) {
  var good = true
  val packet_data = "ffffeeeeffff" + "feeffeefffff" + "8100" + "8000" + "45000098118a000001111b0d0a1e92a6efff" + "fffaf7e6076c00846b664d2d53454152"
  var offset = 0
  var count = 0
  val w = Consts.BIT_WIDTH

  while (count < 5 && offset + w < 4 * packet_data.size) {
    count += 1
    val block = packet_data.substring(offset/4, (offset+w)/4)
    printf("\nOffset = %d, inp = %s\n", offset, block)
    poke(c.io.block,  BigInt(block, 16))
    poke(c.io.offset, offset)
    step(1)

    for (i <- 0 to 1) {
      peek(c.io.out_fields(i))
      peek(c.io.rdys(i))
    }

    offset += w
  }

  true
}

object Parser {
  def main(args: Array[String]): Unit = {

    val fields = Array((16, 64), (32, 64));

    chiselMainTest(args.slice(1, args.length),
      () => Module(new Parser(Consts.BIT_WIDTH, fields))) {
        c => new ParserTests(c)
      }
  }
}

