package Pipeline
import Chisel._
import scala.collection.mutable.HashMap

class Countdown(max: UInt) extends Module {
  val io = new Bundle {
    val in_ready = Bool(INPUT)
    val out_ready = Bool(OUTPUT)
  }

  def wraparound(n: UInt, max: UInt) =
    Mux(n > max, UInt(0), n)

  def counter(processing: Bool, max: UInt) = {
    val x = Reg(init = UInt(0, width=max.getWidth));
    /* This Mux thingy is important.  Else the code is buggy.  Oh man this
     * was hard to catch! */
    x := Mux(processing,
      wraparound(x + UInt(1), max),
      UInt(0))
    x
  }

  val processing = Reg(init = Bool(false))
  val c = counter(processing, max)
  io.out_ready := Bool(false)

  /* Start the counter */
  when (io.in_ready) {
    printf("Test: IO is ready resetting counter to 0\n");
    c := UInt(0)
    io.out_ready := Bool(false);
    processing := Bool(true);
  }

  when (c === max - UInt(1)) {
    io.out_ready := processing;
    processing := Bool(false);
  }
}

class Pipeline(delay: Int) extends Module {
  val BIT_WIDTH = 128
  val io = new Bundle {
    val in_data = UInt(INPUT, width=BIT_WIDTH)
    val out_data = UInt(OUTPUT)
    val in_ready = Bool(INPUT)
    val out_ready = Bool(OUTPUT)
  }

  val c = Module(new Countdown(UInt(delay, width=5)))
  val data = Reg(init = UInt(0))
  /* Latch on to incoming data and store it */
  when (io.in_ready) {
    data := io.in_data + UInt(1)
    printf("Test: Latching on to data %x\n", io.in_data);
  }

  io.out_data := data
  c.io.out_ready <> io.out_ready
  c.io.in_ready <> io.in_ready
}

class SequencePipeline(num: Int, delay_per: Int) extends Module {
  val BIT_WIDTH = 128
  val io = new Bundle {
    val in_data = UInt(INPUT, width=BIT_WIDTH)
    val out_data = UInt(OUTPUT)
    val in_ready = Bool(INPUT)
    val out_ready = Bool(OUTPUT)
  }

  val eles = Array.fill(num) { Module(new Pipeline(delay_per)) }

  eles(0).io.in_data <> io.in_data
  eles(0).io.in_ready <> io.in_ready

  for (i <- 1 to num-1) {
    eles(i-1).io.out_data <> eles(i).io.in_data
    eles(i-1).io.out_ready <> eles(i).io.in_ready
  }

  eles(num-1).io.out_data <> io.out_data
  eles(num-1).io.out_ready <> io.out_ready
}

class SequencePipelineTests(c: SequencePipeline) extends Tester(c, Array(c.io)) {
  defTests {
    val vars = new HashMap[Node, Node]()
    val ovars = new HashMap[Node, Node]()
    vars(c.io.in_data) = Bits("h0000")
    vars(c.io.in_ready) = Bool(true)
    for (i <- 1 to 40) {
      step(vars, ovars)
      if (i == 1) {
        vars(c.io.in_ready) = Bool(false)
      } else if (i == 8) {
        vars(c.io.in_ready) = Bool(true)
        vars(c.io.in_data) = Bits("h0100")
      } else if (i == 9) {
        vars(c.io.in_ready) = Bool(false)
      }

      printf("SequencePipeline Test: Step %d: output is %s\n", i, ovars(c.io.out_ready).litValue())
    }

    true
  }
}

class PipelineTests(c: Pipeline) extends Tester(c, Array(c.io)) {
  defTests {
    val vars = new HashMap[Node, Node]()
    val ovars = new HashMap[Node, Node]()

    var good = true
    vars(c.io.in_data) = Bits("hffff0000ffff")
    vars(c.io.in_ready) = Bool(true)

    for (i <- 1 to 30) {
      step(vars, ovars)
      if (i == 1) {
        vars(c.io.in_ready) = Bool(false)
      } else if (i == 13) {
        printf("Test: sending another data\n");
        vars(c.io.in_data) = Bits("hdeadface")
        vars(c.io.in_ready) = Bool(true)
      } else if (i == 14) {
        vars(c.io.in_ready) = Bool(false)
      }

      printf("Test: Step %d: output is %s\n", i, ovars(c.io.out_ready).litValue())
    }

    true
  }
}

object Pipeline {
  def main(args: Array[String]): Unit = {
    chiselMainTest(args.slice(1, args.length),
      () => Module(new SequencePipeline(6, 5))) {
      c => new SequencePipelineTests(c)
    }
      /*() => Module(new Pipeline())) {
        c => new PipelineTests(c)
      }*/
  }
}

