package Parser
import Chisel._
import EthParser._
import IPParser._
import Consts._

import scala.collection.mutable.HashMap

class Parser extends Module {
  val io = new Bundle {
    val packet = UInt(INPUT, width=Consts.BIT_WIDTH)
    val offset = PacketOffset(INPUT)
    val eth_saddr = EthAddr(OUTPUT)
    val eth_daddr = EthAddr(OUTPUT)
    val eth_type = EthType(OUTPUT)
    val data_offset = PacketOffset(OUTPUT)
    val ip_saddr = IPAddr(OUTPUT)
    val ip_daddr = IPAddr(OUTPUT)
    val ip_hdrlen = UInt(OUTPUT)
  }

  val ethparser = Module(new EthParser())
  ethparser.io.packet <> io.packet
  ethparser.io.offset <> io.offset
  ethparser.io.eth_saddr <> io.eth_saddr
  ethparser.io.eth_daddr <> io.eth_daddr
  ethparser.io.eth_type <> io.eth_type
  val offset1 = ethparser.io.data_offset

  val ipparser = Module(new IPParser())
  ipparser.io.packet <> io.packet
  ipparser.io.offset <> ethparser.io.data_offset
  ipparser.io.hdr_len <> io.ip_hdrlen
  ipparser.io.ip_saddr <> io.ip_saddr
  ipparser.io.ip_daddr <> io.ip_daddr
  val offset2 = ipparser.io.data_offset

  io.data_offset := MuxLookup(ethparser.io.eth_type, offset1,
    Array(Consts.ETH_TYPE_IP -> offset2))

}

class ParserTests(c: Parser) extends Tester(c, Array(c.io)) {
  defTests {
    val vars = new HashMap[Node, Node]()
    var good = true
    val packet_data = Cat(Bits("hffffeeeeffff"),
                          Bits("hfeeffeefffff"),
                          Bits("h8100", width=16),
                          Bits("h0800", width=16),
                          Bits("h45000098118a000001111b0d0a1e92a6efff"),
                          Bits("hfffaf7e6076c00846b664d2d53454152"))
    val fillersize = (Consts.BIT_WIDTH - packet_data.getWidth)
    val filler = Fill(fillersize, UInt("b0"))
    val packet = Cat(packet_data, filler)
    vars(c.io.packet) = packet
    vars(c.io.offset) = UInt(0)
    vars(c.io.eth_saddr) = Bits("hffffeeeeffff")
    vars(c.io.eth_daddr) = Bits("hfeeffeefffff")
    vars(c.io.eth_type) = Bits("h0800")
    vars(c.io.data_offset) = UInt((14 + 2 + 20) * 8 - 1)
    vars(c.io.ip_hdrlen) = Bits("h5") << UInt(5)
    vars(c.io.ip_saddr) = Bits("h0a1e92a6")
    vars(c.io.ip_daddr) = Bits("heffffffa")
    step(vars)
  }
}

object Parser {
  def main(args: Array[String]): Unit = {
    chiselMainTest(args.slice(1, args.length),
      () => Module(new Parser())) {
        c => new ParserTests(c)
      }
  }
}

