package EthParser
import Chisel._
import Consts._

class EthParser extends Module {
  val ETH_TYPE_QINQ = Bits("h8100")

  val io = new Bundle {
    val packet = UInt(INPUT, width=Consts.BIT_WIDTH)
    val offset = PacketOffset(INPUT)
    val eth_saddr = EthAddr(OUTPUT)
    val eth_daddr = EthAddr(OUTPUT)
    val eth_type = EthType(OUTPUT)
    val data_offset = PacketOffset(OUTPUT)
  }

  val msbit = UInt(io.packet.getWidth)
  def getbits(startbit: UInt, width: Int) = {
    /* get width bits from startbit.  MSB is bit 0. */
    val tmp = io.packet >> (msbit - startbit - UInt(width))
    tmp(width-1, 0)
  }

  io.eth_saddr := getbits(io.offset, Consts.ETH_ADDR_WIDTH)
  io.eth_daddr := getbits(io.offset + UInt(Consts.ETH_ADDR_WIDTH), Consts.ETH_ADDR_WIDTH)

  val eth_type1 = getbits(io.offset + UInt(2*Consts.ETH_ADDR_WIDTH), Consts.ETH_TYPE_WIDTH)
  val eth_type2 = getbits(io.offset + UInt(2*Consts.ETH_ADDR_WIDTH + Consts.ETH_TYPE_WIDTH), Consts.ETH_TYPE_WIDTH)

  val doff1 = UInt(2*Consts.ETH_ADDR_WIDTH + Consts.ETH_TYPE_WIDTH - 1)
  val doff2 = UInt(2*Consts.ETH_ADDR_WIDTH + 2*Consts.ETH_TYPE_WIDTH - 1)

  io.eth_type := MuxLookup(eth_type1,
    eth_type1,
    Array(ETH_TYPE_QINQ -> eth_type2))

  io.data_offset := MuxLookup(eth_type1,
    doff1,
    Array(ETH_TYPE_QINQ -> doff2))

}

class EthParserTests(c: EthParser) extends Tester(c, Array(c.io)) {
  defTests {
    true
  }
}

object EthParser {
  def main(args: Array[String]): Unit = {
    chiselMainTest(args.slice(1, args.length),
      () => Module(new EthParser())) {
        c => new EthParserTests(c)
      }
  }
}

