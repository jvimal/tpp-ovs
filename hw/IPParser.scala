package IPParser
import Chisel._
import Consts._
import scala.collection.mutable.HashMap

class IPParser extends Module {
  val io = new Bundle {
    val packet = UInt(INPUT, width=Consts.BIT_WIDTH)
    val offset = PacketOffset(INPUT)
    val ip_saddr = IPAddr(OUTPUT)
    val ip_daddr = IPAddr(OUTPUT)
    val data_offset = PacketOffset(OUTPUT)
    val hdr_len = PacketOffset(OUTPUT)
  }

  val msbit = UInt(io.packet.getWidth)
  def getbits(startbit: UInt, width: Int) = {
    /* get width bits from startbit.  MSB is bit 0. */
    val tmp = io.packet >> (msbit - startbit - UInt(width))
    tmp(width-1, 0)
  }
  val hdr_len = getbits(io.offset + UInt(4), 4) << UInt(5)
  io.hdr_len := hdr_len
  io.ip_saddr := getbits(io.offset + UInt(Consts.WORD * 3, width=20), Consts.IP_ADDR_WIDTH)
  io.ip_daddr := getbits(io.offset + UInt(Consts.WORD * 4, width=20), Consts.IP_ADDR_WIDTH)
  io.data_offset := io.offset + hdr_len
}

class IPParserTests(c: IPParser) extends Tester(c, Array(c.io)) {
  defTests {
    val vars = new HashMap[Node, Node]()
    var good = true
    val packet_data = Cat(Bits("hffffeeeeffff"),
                          Bits("hfeeffeefffff"),
                          Bits("h8100", width=16),
                          Bits("h0800", width=16),
                          Bits("h45000098118a000001111b0d0a1e92a6efff"),
                          Bits("hfffaf7e6076c00846b664d2d53454152"))
    val fillersize = (640 * 8 - packet_data.getWidth)
    val filler = Fill(fillersize, UInt("b0"))
    val packet = Cat(packet_data, filler)
    vars(c.io.packet) = packet
    vars(c.io.offset) = UInt((14 + 2) * 8)
    vars(c.io.ip_saddr) = Bits("h0a1e92a6")
    vars(c.io.ip_daddr) = Bits("heffffffa")
    step(vars)
  }
}

object IPParser {
  def main(args: Array[String]): Unit = {
    chiselMainTest(args.slice(1, args.length),
      () => Module(new IPParser())) {
        c => new IPParserTests(c)
      }
  }
}

