package Consts
import Chisel._

object Consts {
  val BIT_WIDTH = 640 * 8
  val BIT_ADDR_WIDTH = 10
  val ETH_ADDR_WIDTH = 6 * 8
  val ETH_TYPE_WIDTH = 2 * 8
  val IP_ADDR_WIDTH = 4 * 8
  val IP_TYPE_WIDTH = 1 * 8
  val WORD = 4 * 8
  val OFFSET_WIDTH = 16

  val ETH_TYPE_IP = UInt("h0800")
}

object PacketOffset extends UInt {
  def apply(dir: IODirection = null): UInt = {
    val res = new UInt()
    res.create(dir, Consts.OFFSET_WIDTH)
    res
  }
}

object EthAddr extends UInt {
  def apply(dir: IODirection = null): UInt = {
    val res = new UInt()
    res.create(dir, Consts.ETH_ADDR_WIDTH)
    res
  }
}

object EthType extends UInt {
  def apply(dir: IODirection = null): UInt = {
    val res = new UInt()
    res.create(dir, Consts.ETH_TYPE_WIDTH)
    res
  }
}

object IPAddr extends UInt {
  def apply(dir: IODirection = null): UInt = {
    val res = new UInt()
    res.create(dir, Consts.IP_ADDR_WIDTH)
    res
  }
}

