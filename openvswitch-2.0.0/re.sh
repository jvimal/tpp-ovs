#!/bin/bash

echo deleting ctl and pid files
sudo rm -f /usr/local/var/run/openvswitch/ovs-vswitchd.*
sudo rm -f /usr/local/var/run/openvswitch/*.{ctl,pid}
sudo killall -9 ovsdb-server

pushd /usr/local/var/log
sudo rm -rf openvswitch
sudo ln -s /var/log/openvswitch
popd

pushd /usr/local/var/run
sudo rm -rf openvswitch
sudo ln -s /var/run/openvswitch
popd

sudo service openvswitch-switch stop
#sudo service openvswitch-controller stop

sudo rm /usr/local/etc/openvswitch/conf.db
sudo ovsdb-tool create /usr/local/etc/openvswitch/conf.db vswitchd/vswitch.ovsschema

sudo service openvswitch-switch start
#sudo service openvswitch-controller start
