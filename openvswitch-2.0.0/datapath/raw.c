/*
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 */

#include <arpa/inet.h>
#include <linux/if_packet.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <net/if.h>
#include <netinet/ether.h>

#include "dmon2_lib.c"

#define MY_DEST_MAC0	0xff
#define MY_DEST_MAC1	0xff
#define MY_DEST_MAC2	0xff
#define MY_DEST_MAC3	0xff
#define MY_DEST_MAC4	0xff
#define MY_DEST_MAC5	0xff

#define DEFAULT_IF	"h1-eth0"
#define BUF_SIZ		1024

u64 hop_offset(int offset) {
	return offset;
}

u64 switch_mem(int addr) {
	return addr;
}

int fill_tpp(u8 *packet) {
	u8 *ins_ptr;
	int per_hop_bytes = 6 * 8;
	int max_hops = 5;
	int num_instructions = 3;
	int preamble_present = 0;
	u64 preamble = 0;
	int size;

	size = dmon_prepare_buff(packet, per_hop_bytes, max_hops * per_hop_bytes,
				 num_instructions, preamble_present, preamble);

	ins_ptr = packet + size;


	dmon_encode_instruction(ins_ptr,
				dmon_create_instruction(LOAD_REG_HOPMEM,
							REG_EGRESS_QUEUE_ENQUEUED,
							hop_offset(0),
							~0));
	ins_ptr += DMON_INSTRUCTION_SIZE_BYTES;

	dmon_encode_instruction(ins_ptr,
				dmon_create_instruction(LOAD_REG_HOPMEM,
							REG_EGRESS_PORT,
							hop_offset(1),
							~0));
	ins_ptr += DMON_INSTRUCTION_SIZE_BYTES;

	dmon_encode_instruction(ins_ptr,
				dmon_create_instruction(LOAD_REG_HOPMEM,
							REG_SW_DPID,
							hop_offset(2),
							~0));
	ins_ptr += DMON_INSTRUCTION_SIZE_BYTES;

	return	size + max_hops * per_hop_bytes + \
		num_instructions * DMON_INSTRUCTION_SIZE_BYTES;
}

int main(int argc, char *argv[])
{
	int sockfd;
	struct ifreq if_idx;
	struct ifreq if_mac;
	int tx_len = 0;
	char sendbuf[BUF_SIZ];
	struct ether_header *eh = (struct ether_header *) sendbuf;
	struct iphdr *iph = (struct iphdr *) (sendbuf + sizeof(struct ether_header));
	struct sockaddr_ll socket_address;
	char ifName[IFNAMSIZ];

	/* Get interface name */
	if (argc > 1)
		strcpy(ifName, argv[1]);
	else
		strcpy(ifName, DEFAULT_IF);

	/* Open RAW socket to send on */
	if ((sockfd = socket(AF_PACKET, SOCK_RAW, IPPROTO_RAW)) == -1) {
	    perror("socket");
	}

	/* Get the index of the interface to send on */
	memset(&if_idx, 0, sizeof(struct ifreq));
	strncpy(if_idx.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(sockfd, SIOCGIFINDEX, &if_idx) < 0)
	    perror("SIOCGIFINDEX");
	/* Get the MAC address of the interface to send on */
	memset(&if_mac, 0, sizeof(struct ifreq));
	strncpy(if_mac.ifr_name, ifName, IFNAMSIZ-1);
	if (ioctl(sockfd, SIOCGIFHWADDR, &if_mac) < 0)
	    perror("SIOCGIFHWADDR");

	/* Construct the Ethernet header */
	memset(sendbuf, 0, BUF_SIZ);
	/* Ethernet header */
	memcpy(eh->ether_shost,
	       &if_mac.ifr_hwaddr.sa_data,
	       sizeof(eh->ether_shost));

	//bzero(eh->ether_dhost, sizeof(eh->ether_dhost));
	memset(eh->ether_dhost, 0xff, sizeof(eh->ether_dhost));

	/* Ethertype field */
	eh->ether_type = htons(ETH_P_TPP);
	tx_len += sizeof(struct ether_header);

	/* Fill in the TPP */
	{
		int len = fill_tpp(sendbuf + tx_len);
		printf("Adding TPP size %d, reg size %d\n", len, sizeof(struct control_regs));
		tx_len += len;
	}

	/* fill in IP packet */
	char ippkt[] = "\x08\x00\x45\x00\x00\x54\xc9\x4e\x00\x00\x40\x01\x9d\x58\x0a\x00\x00\x02\x0a\x00\x00\x01\x00\x00\x55\x5d\x0d\x91\x00\x03\xdd\x35\xcb\x52\x00\x00\x00\x00\x28\xb3\x0d\x00\x00\x00\x00\x00\x10\x11\x12\x13\x14\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f\x20\x21\x22\x23\x24\x25\x26\x27\x28\x29\x2a\x2b\x2c\x2d\x2e\x2f\x30\x31\x32\x33\x34\x35\x36\x37";
	int len = 5 * 16 + 6;
	memcpy(sendbuf + tx_len, ippkt, len);
	tx_len += len;

	/* Index of the network device */
	socket_address.sll_ifindex = if_idx.ifr_ifindex;
	/* Address length*/
	socket_address.sll_halen = ETH_ALEN;
	/* Destination MAC */
	socket_address.sll_addr[0] = MY_DEST_MAC0;
	socket_address.sll_addr[1] = MY_DEST_MAC1;
	socket_address.sll_addr[2] = MY_DEST_MAC2;
	socket_address.sll_addr[3] = MY_DEST_MAC3;
	socket_address.sll_addr[4] = MY_DEST_MAC4;
	socket_address.sll_addr[5] = MY_DEST_MAC5;

	/* Send packet */
	if (sendto(sockfd, sendbuf, tx_len+256, 0, (struct sockaddr*)&socket_address, sizeof(struct sockaddr_ll)) < 0)
	    printf("Send failed\n");

	return 0;
}
