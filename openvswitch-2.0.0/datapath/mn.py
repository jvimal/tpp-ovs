
from mininet.topo import Topo
from mininet.net import Mininet
from mininet.log import lg, info, setLogLevel
from mininet.util import dumpNodeConnections, quietRun, moveIntf
from mininet.cli import CLI
from mininet.link import TCLink

from subprocess import Popen, PIPE, check_output
from time import sleep, time
from multiprocessing import Process
from argparse import ArgumentParser

import sys
import os
import termcolor as T
import time

setLogLevel('info')

parser = ArgumentParser()
parser.add_argument('--htb', action="store_true", default=False)
parser.add_argument('--tcp', action="store_true", default=False)
parser.add_argument('--carbon', action="store_true", default=False)
FLAGS = parser.parse_args()

class SimpleTopo(Topo):
    def __init__(self):
        super(SimpleTopo, self).__init__()
        self.addNode('h1')
        self.addNode('h2')
        self.addNode('h3')
        self.addSwitch('s1')
        self.addSwitch('s2')
        self.addLink('h1', 's1')
        self.addLink('h2', 's1')
        self.addLink('h3', 's2')
        self.addLink('s1', 's2', bw=10)
        return

def start_carbon(net, hostname):
    host = net.get(hostname)
    os.system('pgrep -f carbon-cache | xargs kill -9')
    return host.popen('/opt/graphite/bin/carbon-cache.py --debug start > /tmp/carbon.log 2>&1',
                      shell=True)

def main():
    os.system("pgrep -f echo.py | xargs kill -9")
    net = Mininet(topo=SimpleTopo(), link=TCLink)
    net.start()
    tccmds = [
        'tc qdisc del dev %s root',
        'tc qdisc add dev %s root handle 1: htb default 1',
        'tc class add dev %s classid 1:1 parent 1: htb rate 10Mbit',
        'tc class add dev %s classid 1:10 parent 1:1 htb rate 5Mbit ceil 10Mbit',
        'tc class add dev %s classid 1:11 parent 1:1 htb rate 5Mbit ceil 10Mbit',
        'tc filter add dev %s parent 1: protocol ip prio 1 u32 match ip protocol 17 0xff flowid 1:10',
        'tc filter add dev %s parent 1: protocol ip prio 2 u32 match ip protocol 6 0xff flowid 1:11',
        ]

    if FLAGS.carbon:
        print "***** Starting carbon..."
        start_carbon(net, 'h1')

    for host in net.hosts:
        if FLAGS.htb:
            for cmd in tccmds:
                intfname = '%s-eth0' % host.name
                cmd = cmd % intfname
                print 'executing --- ', cmd
                host.cmd(cmd)
                host.waitOutput()
        else:
            host.cmd("tc qdisc del dev %s-eth0 root; tc qdisc add dev %s-eth0 root handle 1: tbf rate 10mbit limit 100000 burst 10000" % (host.name, host.name))
            host.waitOutput()
        host.cmd("ip route change 10/8 dev %s-eth0 advmss 1100" % host.name)
        host.waitOutput()
        host.cmd('netserver')
        host.waitOutput()
        # this causes serious bugs/issues. Don't disable gso!!
        #host.cmd("ethtool -K %s-eth0 gso off; ethtool -K %s-eth0 tso off;" % (host.name, host.name))
        #host.waitOutput()
        #host.cmd("iptables -A FORWARD -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --set-mss 1000")
        #host.waitOutput()
        host.popen("python ~/py/echo.py 2>&1 >/tmp/%s.log" % host.name, shell=True)

    for sw in net.switches:
        intfs = []
        for intf in sw.intfs.itervalues():
            if intf.name.startswith('s'):
                intfs.append(intf.name)

        if FLAGS.htb:
            for intfname in intfs:
                for cmd in tccmds:
                    cmd = cmd % intfname
                    print 'executing --- ', cmd
                    sw.cmd(cmd)
                    sw.waitOutput()
    h3 = net.get('h3')
    h3.popen('iperf -s', shell=True)
    sleep(1)
    if FLAGS.tcp:
        net.get('h1').popen('iperf -c %s -t 1000 -i 1 -M 700 > /tmp/iperf-h1.log' % h3.IP(), shell=True)
        net.get('h2').popen('iperf -c %s -t 1000 -i 1 -M 700 > /tmp/iperf-h2.log' % h3.IP(), shell=True)
        print 'starting iperfs...'
    print '*' * 80
    print 'common commands'
    print 'h1 python ~/py/example.py --host 10.0.0.3'
    print 'h1 python ~/py/rcp.py --host 10.0.0.3'
    print 'python ~/py/rcp.py --host 10.0.0.3 --interval 0.05 --time 1000'
    print 'h1 ./a.out'
    print 'h2 netserver'
    print 'h1 netperf -H h2 -l 10 -t TCP_STREAM -D 1s'
    print 'h1 python ~/py/mesh.py'
    print '*' * 80
    CLI(net)
    os.system("pgrep -f echo.py | xargs kill -9")
    os.system("pgrep -f rcp.py | xargs kill -9")
    os.system("killall -9 iperf netserver netperf")
    #net.stop()
    os.system("mn -c")

if __name__ == "__main__":
    main()
