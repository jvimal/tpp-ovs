#!/bin/bash

set -e

if [ -d ovs ]; then
	echo ovs git directory already exists.
	echo please remove it if you want to repatch.
	exit 1
fi

git clone https://github.com/openvswitch/ovs.git
pushd ovs

echo checking out branch
git checkout -b tpp 97a360ced33fe81e29a585aa29b3c7c90c916835

echo patching OVS...
patch -p1 < ../ovs-patches/patch-git

echo Now run recompile_ovs.sh to recompile OVS with TPP
